# Project Momentum

This is a student project from **College LaSalle** in Montreal during the **Fall 2020** semester.

## Gameplay

**Project Momentum** is a 3D platformer in which the player controls a clone that has awaken from its pod. The player must run through a clone producing facility while avoiding multiple traps and obstacles. Once at the final boss, **“The Overseer”**, the player must take it out to set free all of their fellow clones and escape the factory.

## Development
The game was created on Unity version 2020.1.2f1.

## Team Members

Under the supervision of Raphaël Jacques-Fortin, College LaSalle Professor.

|Student                |Position
|----------------|-------------------------------
|Daniel Gomez|Project Leader / Character Artist / Concept Artist
|Fares Benrahmani|Project Leader / Game Designer
|Skilman Meribe|Game Designer
|Chirayu Shaileshkumar Shah|Game Designer
|Mohammedshahzeb Nawab|Game Designer
|Emmanuelle Omann|Level Designers
|Thomas Hervieux|Level Designers
|Donato Mennitto|Level Designers
|Daniel Gagnon|Character Artist / Concept Artist
|Aiganym Baigonys|Character Artist
|Antoine Gautreau|Clothing Artist
|Daniel Désilets|Environment Artist
|Karim Aboushousha|Environment Artist
|Yiwu Yao|Environment Artist
|Clément Gourdeau|Environment Artist
|Grégory Trudeau|Environment Artist
|Rafael Corzo|Technical Artist / VFX Artist
|Eun Jin Han|Illustrator / Concept Artist
|Juliana Maria Dias|Illustrator / Concept Artist
|Catherine Jean|Animator / Concept Artist
 
> All rights reserved to **College LaSalle Montreal 2020**.