﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    #region Singleton
    public static GameManager instance = null;

    private void Awake()
    {
        //Game Manager
        if (instance == null)
            instance = this;
        else if (instance != null)
            Destroy(gameObject);
    }
    #endregion

    [Header("Assign These")]
    public PlayerAbility player;
    [SerializeField] Fade fadeEffect = null;

    [Header("Assign the Default Spawn Location")]
    public Checkpoint spawnPoint;

    [Header("Values")]
    [SerializeField] float timeFadeIn = 2f;
    [SerializeField] float timeFadeOut = 4f;
    public bool killedByCrusher = false;
    public bool gameIsPaused = false;
    public bool gameIsOver = false;
    public bool playerRespawned;

    IEnumerator fadeCoroutine;
    private Character character;

    void Start()
    {
        character = FindObjectOfType<Character>();
        playerRespawned = true;
        //Makes the screen black
        fadeEffect.GetComponent<Image>().enabled = true;
        
        StartCoroutine(TimerFadeIntoGame());
    }

    IEnumerator TimerFadeIntoGame()
    {
        yield return new WaitForSeconds(2f);
        fadeEffect.FadeOut(4f);
    }

    public void RespawnPlayer()
    {
        fadeCoroutine = FadeInOut();
        StartCoroutine(fadeCoroutine);
    }

    IEnumerator FadeInOut()
    {
        if (killedByCrusher)
        {
            fadeEffect.FadeIn(0);
            yield return new WaitForSeconds(timeFadeIn);
        }
        else
        {
            fadeEffect.FadeIn(1);
            yield return new WaitForSeconds(timeFadeIn);
        }

        //Brakes the player
        player.playerInput.MakePlayerStop();

        //Respawns player and turns off controls.
        player.transform.position = spawnPoint.respawnLocation.transform.position;
        //player.playerInput.enabled = false;
        //player.enabled = false;
        playerRespawned = false;
        character.AddMovementInput(0, 0);
        yield return new WaitForSeconds(timeFadeOut);

        //Make sure the player is in the correct possition?
        player.transform.position = spawnPoint.respawnLocation.transform.position;

        //Teleports Camera
        player.cameraObject.transform.position = player.transform.position;

        fadeEffect.FadeOut(2f);

        //Turns on controls
        //player.enabled = true;
        //player.playerInput.enabled = true;
        StartCoroutine(PlayerControlsWaitTime());

        killedByCrusher = false;
    }

    IEnumerator PlayerControlsWaitTime()
    {
        yield return new WaitForSeconds(1f);
        playerRespawned = true;
    }

    public IEnumerator EndGameFadeInOut()
    {
        fadeEffect.FadeIn(0);

        yield return new WaitForSeconds(2f);

        fadeEffect.FadeOut(2f);
    }

    public void StopTime(float time)
    {
        StartCoroutine(StopTime());

        IEnumerator StopTime()
        {
            Time.timeScale = 0f;
            yield return new WaitForSecondsRealtime(time);
            Time.timeScale = 1f;
        }
    }

    public void SlowDownTime(float time, float slowMoStrength)
    {
        StartCoroutine(SlowDownTime());

        IEnumerator SlowDownTime()
        {
            Time.timeScale = slowMoStrength;
            yield return new WaitForSecondsRealtime(time);
            Time.timeScale = 1f;
        }
    }
}