﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndCredits : MonoBehaviour
{
    [Header("End Credit Type")]
    [SerializeField] bool isFromMainMenu = false;

    [Header("Title")]
    [SerializeField] Image titleText = null;
    [SerializeField] float titleTimer = 0f;
    [SerializeField] float timeBeforeFadeInStarts = 1f;
    [SerializeField] float timeToStartTextScroll = 2f;
    [SerializeField] float titleFadeInSpeed = 1f;

    [Header("Controls")]
    [SerializeField] TextMeshProUGUI[] creditControls = null;
    [SerializeField] bool keyWasPressed = false;
    [SerializeField] float controlTimer = 0f;

    [Header("Physical Screen")]
    public MeshRenderer creditScreen = null;
    public Material staticMaterial = null;
    [SerializeField] Material creditLiveFeedMaterial = null;
    [SerializeField] Transform cameraStartPosition = null;
    [SerializeField] Transform cameraEndPosition = null;
    [SerializeField] float cameraMoveSpeed = 1f;
    [SerializeField] float totalTimerBeforeCameraMove = 2f;
    [SerializeField] float totalStaticTimer = 2f;
    [SerializeField] float totalTimerBeforeCreditStart = 2f;

    [Header("Other")]
    [SerializeField] int indexMainMenu = 0;
    [SerializeField] float scrollSpeed = 5f;
    [SerializeField] float normalScrollSpeed = 5f;
    [SerializeField] float fastScrollSpeed = 50f;
    [SerializeField] GameObject textToScroll = null;
    [SerializeField] Transform startPosition = null;
    [SerializeField] Transform endPosition = null;
    [SerializeField] float currentTime = 0f;
    [SerializeField] float timeToEscape = 5f;
    [SerializeField] AudioSource finalSong = null;
    [SerializeField] AudioListener audioListener = null;

    [SerializeField] EndGame endGame = null;

    void Start()
    {
        textToScroll.transform.position = startPosition.position;
        titleText.color = Color.clear;
        for (int i = 0; i < creditControls.Length; i++)
        {
            creditControls[i].color = Color.clear;
        }

        if (isFromMainMenu)
        {
            audioListener.enabled = true;
            StartCoroutine(FadeOut());
        }
    }

    public IEnumerator MoveCameraToPosition()
    {
        creditScreen.material = staticMaterial;

        //Disable the player
        GameManager.instance.player.enabled = false;

        //Unparent camera from the player and then move it to it's start position.
        GameManager.instance.player.cam.transform.parent = cameraStartPosition;
        GameManager.instance.player.cam.transform.position = cameraStartPosition.position;
        GameManager.instance.player.cam.transform.rotation = cameraStartPosition.rotation;

        float currentTimerBeforeCameraMove = 0f;
        while (currentTimerBeforeCameraMove < totalTimerBeforeCameraMove)
        {
            currentTimerBeforeCameraMove += Time.deltaTime;
            yield return null;
        }

        StartCoroutine(StaticTimer());

        while(GameManager.instance.player.cam.transform.position != cameraEndPosition.position)
        {
            GameManager.instance.player.cam.transform.position = Vector3.Lerp(GameManager.instance.player.cam.transform.position, cameraEndPosition.position, Time.deltaTime * cameraMoveSpeed);
            yield return null;
        }
    }

    IEnumerator StaticTimer()
    {
        float currentStaticTimer = 0f;
        while (currentStaticTimer < totalStaticTimer)
        {
            currentStaticTimer += Time.deltaTime;
            yield return null;
        }
        creditScreen.material = creditLiveFeedMaterial;

        float currentTimerBeforeCreditStart = 0f;
        while (currentTimerBeforeCreditStart < totalTimerBeforeCreditStart)
        {
            currentTimerBeforeCreditStart += Time.deltaTime;
            yield return null;
        }

        StartCoroutine(FadeOut());
    }

    IEnumerator FadeOut()
    {
        //Time until Title Fade In Begins
        while (titleTimer < timeBeforeFadeInStarts)
        {
            titleTimer += Time.deltaTime;
            yield return null;
        }
        //Reset Timer
        titleTimer = 0f;

        finalSong.Play();

        if (!isFromMainMenu)
        {
            endGame.bg.gameObject.SetActive(true);
        }

        //Time until Staff Roll
        while (titleTimer < timeToStartTextScroll)
        {
            titleTimer += Time.deltaTime;
            titleText.color = Color.Lerp(titleText.color, Color.white, titleFadeInSpeed * Time.deltaTime);
            yield return null;
        }
        titleText.color = Color.white;
        StartCoroutine(EndCreditsScroll());
    }

    IEnumerator EndCreditsScroll()
    {
        while(textToScroll.transform.position != endPosition.position)
        {
            textToScroll.transform.position = Vector3.MoveTowards(textToScroll.transform.position, endPosition.position, Time.deltaTime * scrollSpeed);

            if (Input.GetKey(KeyCode.Mouse0))
            {
                scrollSpeed = fastScrollSpeed;
            }
            else
            {
                scrollSpeed = normalScrollSpeed;
            }
            yield return null;
        }
        StartCoroutine(FadeInControls());
    }

    void Update()
    {
        //Will fade in the controls
        if (isFromMainMenu)
        {
            if (Input.anyKeyDown)
            {
                if (!keyWasPressed)
                {
                    StartCoroutine(FadeInControls());
                }
                keyWasPressed = true;
            }

            if (Input.GetKey(KeyCode.Escape))
            {
                currentTime += Time.deltaTime;

                if (currentTime >= timeToEscape)
                {
                    //Loads Main Menu, which should be index 0 by default.
                    SceneManager.LoadScene(indexMainMenu);
                }
            }
            else
            {
                currentTime = 0f;
            }
        }
        else
        {
            if (GameManager.instance.gameIsOver)
            {
                if (Input.anyKeyDown)
                {
                    if (!keyWasPressed)
                    {
                        StartCoroutine(FadeInControls());
                    }
                    keyWasPressed = true;
                }

                if (Input.GetKey(KeyCode.Escape))
                {
                    currentTime += Time.deltaTime;

                    if (currentTime >= timeToEscape)
                    {
                        //Loads Main Menu, which should be index 0 by default.
                        SceneManager.LoadScene(indexMainMenu);
                    }
                }
                else
                {
                    currentTime = 0f;
                }
            }
        }
    }

    IEnumerator FadeInControls()
    {
        while (controlTimer < timeToStartTextScroll)
        {
            for (int i = 0; i < creditControls.Length; i++)
            {
                creditControls[i].color = Color.Lerp(creditControls[i].color, Color.white, titleFadeInSpeed * Time.deltaTime);
            }
            yield return null;
        }

        for (int i = 0; i < creditControls.Length; i++)
        {
            creditControls[i].color = Color.white;
        }
    }
}