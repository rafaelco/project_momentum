﻿using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [Header("Fade")]
    [SerializeField] Fade fadeEffect = null;
    [SerializeField] float timeBeforeFadeOut = 2f;
    [SerializeField] float timeToFadeOut = 5f;

    [Header("Camera Animation")]
    [SerializeField] Animator camAnimator = null;
    [SerializeField] float timeBeforeCameraMovesToMainMenu = 2f;

    [Header("Button Fade In")]
    [SerializeField] float timeBeforeButtonsAppear = 10f;
    [SerializeField] Image[] buttonImages = null;
    [SerializeField] float buttonFadeInTime = 1f;

    [Header("Async Loading")]
    //Enable this if you use Async Loading
    AsyncOperation ao = null;

    [Header("Start Button Pressed")]
    [SerializeField] float timeToFadeOutButtons = 1f;
    [SerializeField] float timeBeforeEyesOpen = 3f;
    [SerializeField] AudioSource eyeOpeningSound = null;
    [SerializeField] float timeBeforeZoomOut = 1f;
    [SerializeField] float timeBeforeFadeIn = 6f;
    [SerializeField] float timeToFadeIn = 1f;
    [SerializeField] float timeBeforeLoading = 2f;
    [SerializeField] MeshRenderer playerRenderer = null;
    [SerializeField] Material playerOpenEyes = null;

    [Header("Other")]
    [SerializeField] int indexPlayableScene = 1;
    [SerializeField] int indexCreditsScene = 2;
        
    void Start()
    {
        //Toggles Cursor Visibility
        Cursor.visible = true;

        //Unlocks Cursor
        Cursor.lockState = CursorLockMode.None;

        //This will start the loading of the level in the background.
        StartCoroutine(AsynchronousLoad());

        //Makes the screen black
        fadeEffect.GetComponent<Image>().enabled = true;

        //Makes Button Invisible
        for (int i = 0; i < buttonImages.Length; i++)
        {
            buttonImages[i].GetComponent<Image>().CrossFadeAlpha(0, 1, false);
        }

        //Starts Countdown before camera begins moving.
        StartCoroutine(TimerMoveCameraToMainMenu());

        //Starts Countdown before fade out begins.
        StartCoroutine(TimerFadeOut());

        //Starts Countdown before buttons appear.
        StartCoroutine(TimerMakeButtonsAppear());
    }

    #region First Timers
    IEnumerator TimerMoveCameraToMainMenu()
    {
        yield return new WaitForSeconds(timeBeforeCameraMovesToMainMenu);
        camAnimator.enabled = true;
    }

    IEnumerator TimerFadeOut()
    {     
        yield return new WaitForSeconds(timeBeforeFadeOut);
        fadeEffect.FadeOut(timeToFadeOut);
    }

    IEnumerator TimerMakeButtonsAppear()
    {
        yield return new WaitForSeconds(timeBeforeButtonsAppear);
        for (int i = 0; i < buttonImages.Length; i++)
        {
            buttonImages[i].GetComponent<Image>().CrossFadeAlpha(1, buttonFadeInTime, false);
        }
    }
    #endregion

    public void StartButton()
    {
        //Fades out the Buttons
        for (int i = 0; i < buttonImages.Length; i++)
        {
            buttonImages[i].CrossFadeAlpha(0, timeToFadeOutButtons, false);
        }

        //Centers Camera
        camAnimator.SetBool("Recenter", true);

        //Starts Countdown before eyes open.
        StartCoroutine(TimerBeforeEyesOpen());
    }

    IEnumerator TimerBeforeEyesOpen()
    {
        yield return new WaitForSeconds(timeBeforeEyesOpen);

        //Play Dramatic Sound Effect
        eyeOpeningSound.Play();

        //Switches Sprites
        playerRenderer.material = playerOpenEyes;

        StartCoroutine(TimerBeforeZoomOut());
    }

    IEnumerator TimerBeforeZoomOut()
    {
        yield return new WaitForSeconds(timeBeforeZoomOut);

        //Zooms out the Camera
        camAnimator.SetBool("ZoomOut", true);

        //Start Timer Before Fade In Begins
        StartCoroutine(TimerFadeIn());
    }

    IEnumerator TimerFadeIn()
    {
        yield return new WaitForSeconds(timeBeforeFadeIn);
        fadeEffect.GetComponent<Image>().color = Color.black;
        fadeEffect.FadeIn(timeToFadeIn);

        //Start Timer Before Level is Loaded
        StartCoroutine(TimerLoadingBegins());
    }

    IEnumerator TimerLoadingBegins()
    {
        yield return new WaitForSeconds(timeBeforeLoading);
        LoadButton();
    }

    //This Coroutine will load the level in the background after it's been started. Could be useful if we decide to make a loading screen or need to load the game before the player presses the play button.
    IEnumerator AsynchronousLoad()
    {
        yield return new WaitForSeconds(1);
        ao = SceneManager.LoadSceneAsync(indexPlayableScene);
        //ao = SceneManager.LoadSceneAsync("PlayableGame");

        //This will prevent the loaded scene from playing unless set to true. Like if the player presses the start button.
        ao.allowSceneActivation = false;
    }

    public void LoadButton()
    {
        ////Loads the game scene after pressing play.
        //SceneManager.LoadScene(indexPlayableScene);

        //This will allow the scene to finish loading when the player presses the start button.
        ao.allowSceneActivation = true;
    }

    public void QuitButton()
    {
        Application.Quit();
    }

    public void CreditsButton()
    {
        SceneManager.LoadScene(indexCreditsScene);
    }
}