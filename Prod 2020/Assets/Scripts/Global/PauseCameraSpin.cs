﻿using UnityEngine;

public class PauseCameraSpin : MonoBehaviour
{
    [SerializeField] float xSpeed = 0f;
    [SerializeField] float ySpeed = 0.25f;
    [SerializeField] float zSpeed = 0f;
    [SerializeField] bool isClockWise = false;

    void Start()
    {
        if (isClockWise)
        {
            ySpeed = -ySpeed;
        }

    }

    void Update()
    {
        transform.Rotate(new Vector3(xSpeed, ySpeed, zSpeed));
    }
}