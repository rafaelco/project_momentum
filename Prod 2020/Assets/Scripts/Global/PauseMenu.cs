﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] GameObject pauseMenuPanel = null;
    [SerializeField] int indexMainMenu = 0;
    [SerializeField] bool FPSCounterIsVisible = false;
    [SerializeField] Text textFPS = null;
    float deltaTime = 0f;

    [SerializeField] Volume volume = null;
    [SerializeField] VolumeProfile gameVolumeProfile = null;
    [SerializeField] VolumeProfile pauseVolumeProfile = null;

    void Start()
    {
        pauseMenuPanel.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!GameManager.instance.gameIsOver)
            {
                if (GameManager.instance.gameIsPaused)
                {
                    ResumeGame();
                }
                else
                {
                    PauseGame();
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.F1))
        {
            FPSCounterIsVisible = !FPSCounterIsVisible;
        }

        if (FPSCounterIsVisible)
        {
            textFPS.enabled = true;

            deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
            float fps = 1.0f / deltaTime;
            textFPS.text = Mathf.Ceil(fps).ToString();
        }
        else
        {
            textFPS.enabled = false;
        }
    }

    public void PauseGame()
    {
        //Presumably switches volume profiles to the one with DOF
        volume.profile = pauseVolumeProfile;

        //Moves Camera to Spin Destination
        GameManager.instance.player.cam.transform.position = GameManager.instance.player.destinationSpinCamera.position;
        GameManager.instance.player.cam.transform.rotation = GameManager.instance.player.destinationSpinCamera.rotation;
        GameManager.instance.player.cam.transform.parent = GameManager.instance.player.destinationSpinCamera;

        //Pauses the Camera
        GameManager.instance.player.cameraController.enabled = false;

        //Marks Game as Paused;
        GameManager.instance.gameIsPaused = true;

        //Pauses the Game
        Time.timeScale = 0;

        //Makes the Panel Visible
        pauseMenuPanel.SetActive(true);

        //Pauses Player Input Code and Player Ability
        GameManager.instance.player.playerInput.enabled = false;
        GameManager.instance.player.enabled = false;

        //Toggles Cursor Visibility
        Cursor.visible = true;

        //Unlocks Cursor
        Cursor.lockState = CursorLockMode.None;

        //Pauses Audio Listener
        AudioListener.pause = true;
    }

    public void ResumeGame()
    {
        //Presumably switches volume profiles to the one without DOF
        volume.profile = gameVolumeProfile;

        //Moves Camera to Main Destination
        GameManager.instance.player.cam.transform.position = GameManager.instance.player.destinationMainCamera.position;
        GameManager.instance.player.cam.transform.rotation = GameManager.instance.player.destinationMainCamera.rotation;
        GameManager.instance.player.cam.transform.parent = GameManager.instance.player.destinationCameraSocket;

        //Unpauses the Camera
        GameManager.instance.player.cameraController.enabled = true;

        //Marks Game as unpaused;
        GameManager.instance.gameIsPaused = false;

        //Resumes the Game
        if (GameManager.instance.player.isSlowMo)
        {
            Time.timeScale = GameManager.instance.player.SlowMoScale;
        }
        else
        {
            Time.timeScale = 1;
        }

        //Makes the Panel Visible
        pauseMenuPanel.SetActive(false);

        //Resumes Player Input Code and Player Ability
        GameManager.instance.player.playerInput.enabled = true;
        GameManager.instance.player.enabled = true;

        //Toggles Cursor Visibility
        Cursor.visible = false;

        //Locks Cursor
        Cursor.lockState = CursorLockMode.Locked;

        //Unpauses Audio Listener
        AudioListener.pause = false;
    }

    public void LoadMainMenu()
    {
        //Resumes the Game
        Time.timeScale = 1;

        //Loads Main Menu, which should be index 0 by default.
        SceneManager.LoadScene(indexMainMenu);
    }

    public void QuitGame()
    {
        //Quits Game
        Application.Quit();
    }
}