﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallDetectorScript : MonoBehaviour
{
    public float targetDistance;
    void Start()
    {

    }

    private void FixedUpdate()
    {
        RaycastHit hitInfo;

        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hitInfo))
        {
            targetDistance = hitInfo.distance;
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward), Color.yellow);
        }
    }
}
