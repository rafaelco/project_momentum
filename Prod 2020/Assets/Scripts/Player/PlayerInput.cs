﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Character))]
public class PlayerInput : MonoBehaviour
{
    [SerializeField] CharacterMovement characterMovement;
    private Character character;
    private CharacterAnimationController characterAnimation;
    public KeyCode resetMode;
    bool isNormal = true;
    bool animIsPlaying = false;
    public PlayerKnockBack playerKnockBack;
    private WallChecker wallChecker;
    
    void Start()
    {
        characterMovement = GetComponent<CharacterMovement>();
        character = GetComponent<Character>();
        Cursor.lockState = CursorLockMode.Locked;
        characterAnimation = GetComponent<CharacterAnimationController>();
        playerKnockBack = GetComponent<PlayerKnockBack>();
        wallChecker = FindObjectOfType<WallChecker>();
    }

    void Update()
    {
        if (characterAnimation.animator.GetCurrentAnimatorStateInfo(0).IsName("stop"))  
        {
            //Debug.Log("Stop animation playing");
            animIsPlaying = true;
            characterAnimation.animator.SetBool("stop", false);
        }
        else
        {
            //Debug.Log("Stop animation not playing");
            animIsPlaying = false;
        }

        if (GameManager.instance.player.currentMomentum == ChargeMode.NONE && isNormal == true && animIsPlaying == false)
        {
            if(playerKnockBack.isKnocked == false && GameManager.instance.playerRespawned == true)
            {
                character.AddMovementInput(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"));
                //characterAnimation.animator.SetBool("stop", false);
            }
        }
        else if(GameManager.instance.player.currentMomentum != ChargeMode.NONE)
        {
            character.AddMovementInput(1, 0);
            isNormal = false;
        }

        if(Input.GetKey(resetMode) || Input.GetKey(KeyCode.JoystickButton5))     // Getting controls back
        {
            BrakePlayer();
        }

        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.JoystickButton0))
        {
            character.Jump();
        }
    }

    public void BrakePlayer()
    {
        if (GameManager.instance.player.currentMomentum != ChargeMode.NONE)
        {
            MakePlayerStop();

            characterAnimation.animator.SetBool("stop", true);

            characterMovement.Velocity = Vector3.zero;

            //Plays Brake VFX
            StartCoroutine(BrakeVFX());
        }
    }

    IEnumerator BrakeVFX()
    {
        GameManager.instance.player.skateSparksRight.Play();
        GameManager.instance.player.skateSparksLeft.Play();

        yield return new WaitForSeconds(GameManager.instance.player.sparksTime);

        GameManager.instance.player.skateSparksRight.Stop();
        GameManager.instance.player.skateSparksLeft.Stop();
    }

    public void MakePlayerStop()
    {
        GameManager.instance.player.currentMomentum = ChargeMode.NONE;

        StartCoroutine(WaitForSeconds());

        //Change Player New FOV
        GameManager.instance.player.destinationFOV = GameManager.instance.player.normalFOV;

        //Resets FOV change time and starts coroutine if there isn't one already present.
        GameManager.instance.player.StartFOVCoroutine();

        //Turns off the Boost Aura
        GameManager.instance.player.boostAura.SetActive(false);

        //Skate Trails VFX color
        GameManager.instance.player.skateTrailsRight.GetComponent<Renderer>().material.SetColor("Color_4D3B453A", GameManager.instance.player.colorNormal);
        GameManager.instance.player.skateTrailsLeft.GetComponent<Renderer>().material.SetColor("Color_4D3B453A", GameManager.instance.player.colorNormal);
        GameManager.instance.player.skinnedMeshRenderer.materials[4].SetColor("Color_316403B1", GameManager.instance.player.colorNormal);

        //Set player as not boosting
        GameManager.instance.player.isBoosting = false;

        ////Sound
        GameManager.instance.player.soundBoost.Stop();
    }

    IEnumerator WaitForSeconds()
    {
        yield return new WaitForSeconds(0.5f);
        isNormal = true;
    }
}