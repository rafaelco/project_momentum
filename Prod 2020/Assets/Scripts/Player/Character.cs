﻿using UnityEngine;

public class Character : MonoBehaviour
{
    private float forwardInput;
    private float rightInput;

    private Vector3 velocity;
    Vector3 translation;

    Vector3 camFwd;
    Vector3 camRight;

    public CameraController cameraController;
    public CharacterMovement characterMovement;
    public CharacterAnimationController characterAnimation;
    public CharacterIK characterIK;

    public void AddMovementInput(float forward, float right)        // update movement inputs
    {
        camFwd = Camera.main.transform.forward;
        camRight = Camera.main.transform.right;

        translation = forward * camFwd;
        translation += right * camRight;
        translation.y = 0;
        if (translation.magnitude>0)
        {
            velocity = translation;
        }
        else
        {
            velocity = Vector3.zero;
        }
        characterMovement.Velocity = translation;
    }

    internal void Jump()
    {
        characterMovement.Jump();
        characterAnimation.Jump();
        characterIK.TemporarilyDisableIK(2);

        characterMovement.OnLanded += characterAnimation.Land;
    }

    public float getVelocity()
    {        
        return characterMovement.Velocity.magnitude;
    }

    public void ToggleRun()
    {
        if (characterMovement.GetMovementMode()!= MovementMode.Running)
        {
            characterMovement.SetMovementMode(MovementMode.Running);
            //characterAnimation.SetMovementMode(MovementMode.Running);
        }
        else
        {
            characterMovement.SetMovementMode(MovementMode.Running);
            //characterAnimation.SetMovementMode(MovementMode.Running);
        }
    }

    public void TogglePunching()
    {
        if (characterMovement.GetMovementMode() != MovementMode.Punching)
        {
            characterMovement.SetMovementMode(MovementMode.Punching);
            //characterAnimation.SetMovementMode(MovementMode.Punching);
        }
        else
        {
            characterMovement.SetMovementMode(MovementMode.Walking);
            //characterAnimation.SetMovementMode(MovementMode.Walking);
        }
    }

    public void ToggleSprint(bool enable)
    {
        if (enable)
        {
            characterMovement.SetMovementMode(MovementMode.Sprinting);
            //characterAnimation.SetMovementMode(MovementMode.Sprinting);
        }
        else
        {
            characterMovement.SetMovementMode(MovementMode.Running);
            //characterAnimation.SetMovementMode(MovementMode.Running);
        }
    }
}