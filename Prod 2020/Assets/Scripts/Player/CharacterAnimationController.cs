﻿using UnityEngine;

[RequireComponent(typeof(Character))]
public class CharacterAnimationController : MonoBehaviour
{

    public Animator animator;
    private Character character;
    private float speed = 5f;
    CharacterMovement characterMovement;
    //public Animation animation;

    void Start()
    {
        character = GetComponent<Character>();
        characterMovement = GetComponent<CharacterMovement>();
        //Time.timeScale = 0.1f;
    }

    void LateUpdate()
    {
        if (animator == null)
        {
            Debug.LogWarning("No Valid Animator");
            return;
        }

        speed = Mathf.SmoothStep(speed, character.getVelocity(), Time.deltaTime * 40);

        animator.SetFloat("Velocity", speed);

        PlayerFalling();

        if (characterMovement.m_IsGrounded == true)
        {
            animator.SetBool("Land2", true);
        }
        else
        {
            animator.SetBool("Land2", false);
        }
    }

    internal void Jump()
    {
        if(characterMovement.m_IsGrounded == true)
        {
            animator.SetTrigger("Jump");
        }
    }

    public void Land()
    {
        // animator.SetTrigger("Land");
    }

    void PlayerFalling()
    {
        if(characterMovement.m_IsGrounded == false && characterMovement.Velocity.y <= -10.7f)
        {
            animator.SetBool("isFalling", true);
            //Debug.Log(characterMovement.Velocity.x + characterMovement.Velocity.y + characterMovement.Velocity.z);
        }
        else
        {
            animator.SetBool("isFalling", false);
        }
    }
}
