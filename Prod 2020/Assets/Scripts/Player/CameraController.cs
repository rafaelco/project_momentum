﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] float cameraSmoothingFactor = 1;
    [SerializeField] float lookUpMax = 45;
    [SerializeField] float lookUpMin = -45;

    [SerializeField] Transform t_camera = null;
    public LayerMask IgnoreLayer;

    private Quaternion camRotation;
    private RaycastHit hit;
    private Vector3 camera_offset;

    void Start()
    {
        camRotation = transform.localRotation;
        camera_offset = t_camera.localPosition;
    }

    void Update()
    {
        //Look Up/Down
        camRotation.x += Input.GetAxis("Mouse Y") * cameraSmoothingFactor * (-1);
        //Look Left/Right
        camRotation.y += Input.GetAxis("Mouse X") * cameraSmoothingFactor;

        //Clamping max and min look up/down angles 
        camRotation.x = Mathf.Clamp(camRotation.x, lookUpMin, lookUpMax);

        transform.localRotation = Quaternion.Euler(camRotation.x, camRotation.y, camRotation.z);

        if (!GameManager.instance.player.characterMovement.playerFollowingTrajectory)
        {
            if (!GameManager.instance.player.playerInput.playerKnockBack.isKnocked)
            {
                if (!GameManager.instance.gameIsOver)
                {
                    if (Physics.Linecast(transform.position, transform.position + transform.localRotation * camera_offset, out hit, ~IgnoreLayer))
                    {
                        t_camera.localPosition = new Vector3(0, 0, -Vector3.Distance(transform.position, hit.point));
                    }
                    else
                    {
                        t_camera.localPosition = Vector3.Lerp(t_camera.localPosition, camera_offset, Time.deltaTime);
                    }
                }
            }
        }
    }
}