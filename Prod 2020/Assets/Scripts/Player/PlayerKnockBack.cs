﻿using System.Collections;
using UnityEngine;
using DG.Tweening;
using EZCameraShake;

public class PlayerKnockBack : MonoBehaviour
{
    [SerializeField] GameObject transformSocket = null;
    CharacterAnimationController characterAnimationController;
    public bool isKnocked;
    [SerializeField] float controlReturnTime = 1f;

    [Header("Camera Values")]
    [SerializeField] float camSpeedKnockback = 1;
    [SerializeField] float camNormalSpeedTime = 2f;

    [Header("Camera Shake")]
    [SerializeField] float shakerMagnitude = 4f;
    [SerializeField] float shakerRoughness = 4f;
    [SerializeField] float shakerFadeInTime = 0.1f;
    [SerializeField] float shakerFadeOutTime = 2f;

    [Header("VFX Variables")]
    [SerializeField] Material knockbackVFXMaterial = null;
    [SerializeField] float lowEmissiveVFX = 0f;
    [SerializeField] float highEmissiveVFX = 2f;
    [SerializeField] float speedVFX = 10f;

    void Start()
    {
        characterAnimationController = GetComponent<CharacterAnimationController>();
        knockbackVFXMaterial = GameManager.instance.player.knockbackVFX.GetComponent<MeshRenderer>().material;
    }

    void Update()
    {
        if(isKnocked == false)
        {
            characterAnimationController.animator.SetBool("isKnockBack", false);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (GameManager.instance.player.currentMomentum != ChargeMode.NONE)
        {
            if (collision.gameObject.tag != "IsNotKnockable")
            {
                KnockBack();
            }
            else
            {
                isKnocked = false;
            }
        }
    }

    public void LaserKnockBack()
    {
        KnockBack();
    }

    void KnockBack()
    {
        //Handles the Knockback VFX
        StartCoroutine(KnockBackVFXTimer());

        //Will Shake the Camera
        CameraShaker.Instance.ShakeOnce(shakerMagnitude, shakerRoughness, shakerFadeInTime, shakerFadeOutTime);

        //Dotween for knockback animation
        transform.DOJump(transformSocket.transform.position, 0.5f, 0, 1f);
        characterAnimationController.animator.SetBool("isKnockBack", true);
        isKnocked = true;
        StartCoroutine(WaitForControls());
        GameManager.instance.player.playerInput.MakePlayerStop();

        //Changes the Camera Speed
        //GameManager.instance.player.keepCameraWithPlayer.camSpeed = GameManager.instance.player.cameraSpeedSlow;
        GameManager.instance.player.keepCameraWithPlayer.camSpeed = camSpeedKnockback;

        StartCoroutine(ReturnToNormalCamera());
    }

    IEnumerator KnockBackVFXTimer()
    {
        float currentVFXFloat = lowEmissiveVFX;

        //Will display the knockback VFX
        GameManager.instance.player.knockbackVFX.SetActive(true);

        //Set Emissive Intensity to Low Value
        knockbackVFXMaterial.SetFloat("Vector1_F583F2FB", lowEmissiveVFX);

        while (knockbackVFXMaterial.GetFloat("Vector1_F583F2FB") != highEmissiveVFX)
        {
            currentVFXFloat = Mathf.MoveTowards(currentVFXFloat, highEmissiveVFX, Time.deltaTime * speedVFX);
            knockbackVFXMaterial.SetFloat("Vector1_F583F2FB", currentVFXFloat);
            yield return null;
        }

        while (knockbackVFXMaterial.GetFloat("Vector1_F583F2FB") != lowEmissiveVFX)
        {
            currentVFXFloat = Mathf.MoveTowards(currentVFXFloat, lowEmissiveVFX, Time.deltaTime * speedVFX);
            knockbackVFXMaterial.SetFloat("Vector1_F583F2FB", currentVFXFloat);
            yield return null;
        }

        //Will hide the knockback VFX
        GameManager.instance.player.knockbackVFX.SetActive(false);
    }

    IEnumerator WaitForControls()
    {
        yield return new WaitForSeconds(controlReturnTime);

        characterAnimationController.animator.SetBool("isKnockBack", false);
        isKnocked = false;

        //TODO: The reason the following code is the way it is is because of jittering. There would not be a need for it if the jittering was fixed.
        GameManager.instance.player.keepCameraWithPlayer.camSpeed = 2f;
        yield return new WaitForSeconds(0.2f);
        GameManager.instance.player.keepCameraWithPlayer.camSpeed = 10f;
    }

    IEnumerator ReturnToNormalCamera()
    {
        yield return new WaitForSeconds(camNormalSpeedTime);
        
        //Changes the Camera Speed
        GameManager.instance.player.keepCameraWithPlayer.camSpeed = GameManager.instance.player.cameraSpeedNormal;
    }
}