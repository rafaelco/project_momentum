﻿using EZCameraShake;
using System.Collections;
using UnityEngine;
using UnityEngine.VFX;

public enum ChargeMode { NONE, LOW, MEDIUM, HIGH };

public class PlayerAbility : MonoBehaviour
{
    public ChargeMode currentMomentum;

    IEnumerator coroutineFOV = null;
    IEnumerator coroutineBoostVFX = null;

    public bool isBoosting = false;
    [SerializeField] bool coroutineFOVIsRunning = false;
    [SerializeField] bool coroutineStartBoostingIsRunning = false;

    [Header("Objects")]
    public Camera cam = null;
    public PlayerInput playerInput = null;
    public CameraController cameraController = null;
    public CharacterMovement characterMovement = null;
    public KeepCameraWithPlayer keepCameraWithPlayer = null;
    public Transform destinationCameraSocket = null;
    public Transform destinationMainCamera = null;
    public Transform destinationSpinCamera = null;
    public Collider collisionDirection = null;
    public Collider collisionRightHand = null;

    [Header("VFX")]
    [SerializeField] VisualEffect startBoosting = null;
    public GameObject boostAura = null;
    public GameObject knockbackVFX = null;
    public SkinnedMeshRenderer skinnedMeshRenderer = null;
    public ParticleSystem skateTrailsRight = null;
    public ParticleSystem skateTrailsLeft = null;
    public VisualEffect skateSparksRight = null;
    public VisualEffect skateSparksLeft = null;
    public float sparksTime = 2f;
    public ParticleSystem hitParticle = null;
    public float hitParticleTime = 1f;

    [Header("Sounds")]
    public AudioSource soundBoost = null;
    public AudioSource skateNormal = null;

    [Header("Colors")]
    [ColorUsage(true, true)]
    public Color colorNormal = new Color(0, 150, 255, 255);
    [ColorUsage(true, true)]
    [SerializeField] Color colorHigh = new Color(255, 168, 0, 255);

    [Header("FOV Values")]
    public float destinationFOV = 0f;
    public float normalFOV = 50f;
    [SerializeField] float highFOV = 110f;

    [Header("Current and total time the FOV can take to change")]
    public float currentDurationFOV = 0f;
    [SerializeField] float totalDurationFOV = 1f;

    public GameObject cameraObject = null;
    public bool isSlowMo = false;
    public float SlowMoScale = 0.25f;
    public float cameraSpeedSlow = 18f;
    public float cameraSpeedNormal = 50f;

    public CameraShaker cameraShaker = null;

    void Start()
    {
        hitParticle.Stop();
        skateSparksRight.Stop();
        skateSparksLeft.Stop();

        cam.fieldOfView = normalFOV;
        startBoosting.Stop();
        boostAura.SetActive(false);
        skateTrailsRight.GetComponent<Renderer>().material.SetColor("Color_4D3B453A", colorNormal);
        skateTrailsLeft.GetComponent<Renderer>().material.SetColor("Color_4D3B453A", colorNormal);
        skinnedMeshRenderer.materials[4].SetColor("Color_316403B1", colorNormal);
    }

    void Update()
    {
        //DEBUG //Triggers the slow motion code.
        if (Input.GetKeyDown(KeyCode.F3))
        {
            if (isSlowMo)
            {
                NoMo(false);
            }
            else
            {
                SlowMo();
            }
        }

        if (Input.GetKeyDown(KeyCode.Mouse0) && !GameManager.instance.gameIsPaused)
        {
            //Change Player Speed
            currentMomentum = ChargeMode.HIGH;

            //Change Player New FOV
            destinationFOV = highFOV;

            if (!isBoosting)
            {
                ////VFX
                //Changes Start Boost VFX color then activates it.
                startBoosting.SetVector4("ParticleColour", colorHigh);
                StartBoostVFX();

                //Changes Boost Aura VFX color then activates it.
                boostAura.GetComponent<MeshRenderer>().material.SetColor("Color_EDB25D5C", colorHigh);
                boostAura.SetActive(true);

                //Skate Trails VFX color
                skateTrailsRight.GetComponent<Renderer>().material.SetColor("Color_4D3B453A", colorHigh);
                skateTrailsLeft.GetComponent<Renderer>().material.SetColor("Color_4D3B453A", colorHigh);
                skinnedMeshRenderer.materials[4].SetColor("Color_316403B1", colorHigh);

                ////Sound
                soundBoost.Play();
            }

            //Resets FOV change time and starts coroutine if there isn't one already present.
            StartFOVCoroutine();

            //Mark player as boosting
            isBoosting = true;
        }
    }

    public void StartFOVCoroutine()
    {
        currentDurationFOV = 0f;

        //Check if the FOV Coroutine is currently running
        if (!coroutineFOVIsRunning)
        {
            //Store Coroutine
            coroutineFOV = ChangeFOVCoroutine();

            //Start Coroutine
            StartCoroutine(coroutineFOV);

            //Mark Coroutine as Running
            coroutineFOVIsRunning = true;
        }
    }

    IEnumerator ChangeFOVCoroutine()
    {
        while (currentDurationFOV <= totalDurationFOV)
        {
            currentDurationFOV += Time.deltaTime;

            cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, destinationFOV, currentDurationFOV / totalDurationFOV);

            yield return null;
        }
        cam.fieldOfView = destinationFOV;

        //Mark Coroutine as Not Running
        coroutineFOVIsRunning = false;

        //Stops Current Coroutine
        StopCoroutine(coroutineFOV);
    }

    void StartBoostVFX()
    {
        if (!coroutineStartBoostingIsRunning)
        {
            //Store Coroutine
            coroutineBoostVFX = StartBoostingVFXCoroutine();

            //Start Coroutine
            StartCoroutine(coroutineBoostVFX);

            //Mark Coroutine as Running
            coroutineStartBoostingIsRunning = true;
        }
    }

    IEnumerator StartBoostingVFXCoroutine()
    {
        startBoosting.Play();
        yield return new WaitForSeconds(0.1f);
        startBoosting.Stop();

        //Mark Coroutine as Not Running
        coroutineStartBoostingIsRunning = false;

        //Stops Current Coroutine
        StopCoroutine(coroutineBoostVFX);
    }

    public void SlowMo()
    {
        Time.timeScale = SlowMoScale;
        isSlowMo = true;
        keepCameraWithPlayer.camSpeed = cameraSpeedSlow;
    }

    public void NoMo(bool isTrajectory)
    {
        Time.timeScale = 1f;
        isSlowMo = false;

        if (!isTrajectory)
        {
            keepCameraWithPlayer.camSpeed = cameraSpeedNormal;
        }
    }
}