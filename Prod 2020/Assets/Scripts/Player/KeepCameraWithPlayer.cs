﻿using UnityEngine;

public class KeepCameraWithPlayer : MonoBehaviour
{
    [SerializeField] GameObject playerCharacter = null;
    public float camSpeed = 50f;

    void LateUpdate()
    {
        transform.position = Vector3.MoveTowards(transform.position, playerCharacter.transform.position, Time.deltaTime * camSpeed);
    }
}