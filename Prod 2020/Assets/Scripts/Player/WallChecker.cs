﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallChecker : MonoBehaviour
{
    public float[] DetectorDistance;
    private CharacterMovement characterMovement;
    public WallDetectorScript[] WallDetectorObjects; // = new float[2];
    public bool wallInFront = false;
    private Character character;
    private void Start()
    {
        WallDetectorObjects = GetComponentsInChildren<WallDetectorScript>();
        characterMovement = FindObjectOfType<CharacterMovement>();
        character = FindObjectOfType<Character>();
    }

    private void Update()
    {
        
        if(WallDetectorObjects[0].targetDistance <= 0.13f && WallDetectorObjects[1].targetDistance <= 0.13f && WallDetectorObjects[2].targetDistance <= 0.13f)
        {
            //characterMovement.Velocity = new Vector3(0, -10f, 0);
            //wallInFront = true;
            //StartCoroutine(WaitForBool());
        } 
    }

    IEnumerator WaitForBool()
    {
        yield return new WaitForSeconds(10f);
        wallInFront = false;
    }
}
