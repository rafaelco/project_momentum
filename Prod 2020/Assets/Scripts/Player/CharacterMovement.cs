﻿using System.Collections;
using UnityEngine;

public enum MovementMode { Walking, Running, Punching, Sprinting };

[RequireComponent(typeof(Rigidbody))]
public class CharacterMovement : MonoBehaviour
{
    public Transform t_mesh;
    [Header("Player Speed")]
    public float maxSpeed = 5f;
    private float walkSpeed = 6.7f;
    public float lowChargeSpeed = 8f;
    public float mediumChargeSpeed = 10f;
    public float highChargeSpeed = 15f;

    public bool m_IsGrounded;
    [SerializeField] bool debugInfiniteJumping = false;
    int m_DropdownValue;
    public float m_GroundCheckDistance = 1f;

    public float distToGround = 0.1f;

    [Header("Jump Power")]
    public float jumpNormal = 600;
    public float jumpLow = 700;
    public float jumpMedium = 800;
    public float jumpHigh = 900;

    private float smoothSpeed = 10f;
    private float rotationSpeed = 20;

    new Rigidbody rigidbody;
    new Collider collider;

    private MovementMode movementMode;

    private Vector3 velocity;
    private Vector3 actualVelocity;
    private Vector3 charPos;

    //Delegate()
    public delegate void OnLandedDelegate();
    //Event
    public event OnLandedDelegate OnLanded;

    public bool playerOnRamp = false;
    private bool inAir;
    public bool playerFollowingTrajectory = false;

    void Start()
    {
        //Initialize rigidbody reference
        rigidbody = GetComponent<Rigidbody>();          
        SetMovementMode(MovementMode.Walking);
        charPos = transform.position;
        collider = GetComponent<Collider>();

        //Get the distance to ground
        distToGround = collider.bounds.extents.y;      
        maxSpeed = walkSpeed;

    }

    void Update()
    {
        DebugMode();
    }

    void FixedUpdate()
    {

        //horizontalVelocity = Vector2.Dot(rigidbody.velocity, Vector2.right);
        //Debug.Log(horizontalVelocity);
        //verticalVelocity = Vector2.Dot(rigidbody.velocity, Vector2.up);
        //Debug.Log(verticalVelocity);
        /*

        if(Input.GetAxis("Vertical") == 0 && Input.GetAxis("Horizontal") == 0)
        {
            rigidbody.velocity = new Vector3(0, rigidbody.velocity.y, 0);
        }
        */
        CheckGround();
        actualVelocity = Vector3.Lerp(actualVelocity, (transform.position - charPos) / Time.deltaTime, Time.deltaTime * 50);
        charPos = transform.position;

        ChargeSpeed();

        if (velocity.magnitude > 0)
        {
            rigidbody.velocity = new Vector3(velocity.normalized.x * smoothSpeed, rigidbody.velocity.y, velocity.normalized.z * smoothSpeed);
            smoothSpeed = Mathf.Lerp(smoothSpeed, maxSpeed, Time.deltaTime * 20);

            //This is retarded, but this fixes the jittering issue.
            if (!playerFollowingTrajectory)
            {
                t_mesh.rotation = Quaternion.Lerp(t_mesh.rotation, Quaternion.LookRotation(velocity), Time.deltaTime * rotationSpeed);
            }
        }
        else if (velocity.magnitude < 0)
        {
            smoothSpeed = Mathf.Lerp(smoothSpeed, 0, Time.deltaTime * 20);
        }

        if(inAir)
        {
            if(m_IsGrounded)
            {
                inAir = false;
                OnLanded();
            }
        }
    }

    //This is retarded, but this fixes the jittering issue and will make the player face the same direction as the ramp.
    public IEnumerator RampRotation(GameObject ramp)
    {
        while (playerFollowingTrajectory)
        {
            yield return new WaitForEndOfFrame();
            t_mesh.rotation = Quaternion.Lerp(t_mesh.rotation, Quaternion.LookRotation(ramp.transform.forward), Time.deltaTime * rotationSpeed);
        }
    }

    void DebugMode()
    {
        if (Input.GetKeyDown(KeyCode.F2))
        {
            debugInfiniteJumping = !debugInfiniteJumping;
        }
    }

    //Checks gound distance
    void CheckGround()
    {
        RaycastHit hitInfo;
        if(Physics.Raycast(transform.position + (Vector3.up * 0.05f), Vector3.down, out hitInfo, m_GroundCheckDistance))
        {
           m_IsGrounded = true;
           Debug.DrawRay(transform.position + (Vector3.up * 0.05f), Vector3.down, Color.yellow, m_GroundCheckDistance);
           //Debug.Log("Grounded");
        }
        else
        {
            m_IsGrounded = false;
            Debug.DrawRay(transform.position + (Vector3.up * 0.05f), Vector3.down, Color.yellow, m_GroundCheckDistance);
            //Debug.Log("NotGrounded");
        }
    }

    internal void Jump()
    {
        if (playerFollowingTrajectory)
        {
            if (GameManager.instance.player.currentMomentum == ChargeMode.LOW)
            {
                rigidbody.AddForce(Vector3.up * jumpLow);
            }
            else if (GameManager.instance.player.currentMomentum == ChargeMode.MEDIUM)
            {
                rigidbody.AddForce(Vector3.up * jumpMedium);
            }
            else if (GameManager.instance.player.currentMomentum == ChargeMode.HIGH)
            {
                rigidbody.AddForce(Vector3.up * jumpHigh) ;
            }
        }
        else
        {
            if (m_IsGrounded == true)
            {
                rigidbody.AddForce(Vector3.up * jumpNormal);
            }
            else if (debugInfiniteJumping)
            {
                rigidbody.AddForce(Vector3.up * jumpNormal);
            }
        }

        inAir = true;
    }

    public Vector3 Velocity { get => actualVelocity; set => velocity = value; }
   

    public void SetMovementMode(MovementMode mode)
    {
        movementMode = mode;
    }

    public MovementMode GetMovementMode()
    {
        return movementMode;
    }

    public void ChargeSpeed()
    {
        //Debug.Log(playerAbility.chargeMode);
        if (GameManager.instance.player.currentMomentum == ChargeMode.NONE)
        {
            maxSpeed = walkSpeed;
        }

        else if (GameManager.instance.player.currentMomentum == ChargeMode.LOW)
        {
            maxSpeed = lowChargeSpeed;
        }

        else if (GameManager.instance.player.currentMomentum == ChargeMode.MEDIUM)
        {
            maxSpeed = mediumChargeSpeed;
        }

        else if (GameManager.instance.player.currentMomentum == ChargeMode.HIGH)
        {
            maxSpeed = highChargeSpeed;
        }
    }
}