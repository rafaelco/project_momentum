﻿using UnityEngine;

public class GlassCollisionReaction : MonoBehaviour
{
    [TextArea(minLines: 2, maxLines: 4, order = 0)]
    public string glassCollisionInstructions = "This script makes the shards of the breakable wall fly when the player collides with it. Hover over a value to see its tooltip on what it does";

    [Header("Shard Explosion Values", order = 1)]
    [Tooltip("this value dictates how powerful the 'push' will be. Default value and above recommended")]
    [SerializeField] float explosionStrength = 5000f;
    //[Tooltip("how wide the explosion radius will be applied to the shard")]
    //[SerializeField] float explosionRadius = 100f;

    void OnCollisionEnter(Collision collision)
    {
        //check if the collided object has the same layer tag of this object (self)
        if (collision.gameObject.layer == gameObject.layer)
        {
            Rigidbody colliderRB = collision.gameObject.GetComponent<Rigidbody>();
            //gets the collision point of where the collision exactly happened between the glass shard and this object (self)
            //Vector3 collisionPoint = collision.GetContact(0).thisCollider.transform.position;

            //pushes the object in the forward direction of the collision point at the speed of the explosion strength and how wide the explosion radius is set in the inspector.
            //colliderRB.AddExplosionForce(explosionStrength, collisionPoint, explosionRadius); //Previous Version
            colliderRB.AddRelativeForce(collision.transform.forward * explosionStrength);;
        }
    }
}