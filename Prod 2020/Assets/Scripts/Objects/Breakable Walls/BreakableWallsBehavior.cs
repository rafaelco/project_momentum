﻿using UnityEngine;
using EZCameraShake;

enum TimeFunction { Nothing, StopTime, SlowTime }; //Unity REQUIRES me to do this

public class BreakableWallsBehavior : MonoBehaviour
{
    [Header("Hack Fix", order = -1)]
    [SerializeField] bool bypassBoost = false;

    [Header("Time Manipulation Parameters", order = 0)]
    [Tooltip("the function this object will do this when the player")]
    [SerializeField] TimeFunction objectFunction = TimeFunction.StopTime;

    [Space]

    [Tooltip("how long will time be stopped")]
    [SerializeField] float timeStopTimer = 0.025f;

    [Space]

    [Tooltip("how long will the slow-mo last")]
    [SerializeField] float slowMoTimer = 1f;
    [Tooltip("how strong the slow-mo will be")]
    [SerializeField] float slowMoStrength = 0.8f;

    [Header("Wall Parameters", order = 1)]
    [Tooltip("place here what object replaces the wall when the player collides with it")]
    [SerializeField] GameObject destroyedWall = null;
    [Tooltip("where the wall spawns")]
    [SerializeField] GameObject spawnPivot = null;

    [Header("Camera Shake")]
    [SerializeField] float shakerMagnitude = 4f;
    [SerializeField] float shakerRoughness = 4f;
    [SerializeField] float shakerFadeInTime = 0.1f;
    [SerializeField] float shakerFadeOutTime = 2f;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if(collision.collider.GetComponent<PlayerAbility>() != null)
            {
                GameManager.instance.player = collision.collider.GetComponent<PlayerAbility>();

                //check if player's charging speed is at High
                if (GameManager.instance.player.currentMomentum == ChargeMode.HIGH || bypassBoost)
                {
                    //Will Shake the Camera
                    CameraShaker.Instance.ShakeOnce(shakerMagnitude, shakerRoughness, shakerFadeInTime, shakerFadeOutTime);

                    //Spawn Interactable Destroyed Wall
                    GameObject replacementWall = Instantiate(destroyedWall, spawnPivot.transform.position, transform.rotation);
                    replacementWall.transform.localScale = transform.lossyScale;

                    //call time manipulation function
                    ManipulateTime();

                    //remove self
                    Destroy(gameObject);
                    Destroy(gameObject.transform.parent.gameObject);
                }
            }
        }
    }

    void ManipulateTime()
    {
        switch (objectFunction)
        {
            case TimeFunction.Nothing:
                break;

            case TimeFunction.StopTime:
                GameManager.instance.StopTime(timeStopTimer);
                break;

            case TimeFunction.SlowTime:
                GameManager.instance.SlowDownTime(slowMoTimer, slowMoStrength);
                break;
        }

        //Remove self
        Destroy(gameObject);
        Destroy(gameObject.transform.parent.gameObject);
    }
}