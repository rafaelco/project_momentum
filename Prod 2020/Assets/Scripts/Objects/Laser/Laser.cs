﻿using System.Collections;
using UnityEngine;

public class Laser : MonoBehaviour
{
	[Header("Laser Controls")]
	[SerializeField] bool laserIsOnAtStart = true;
	[SerializeField] bool isMovingOnStart = false;
	[SerializeField] GameObject[] waypoints = null;
	[Range(1.0f, 5.0f)]
	[SerializeField] float laserMoveSpeed = 1;
	[SerializeField] int currentDestination = 0;

	[Header("Colors")]
	[ColorUsage(true, true)]
	[SerializeField] Color laserColorBright = new Color(191, 0, 0, 255);
	[ColorUsage(true, true)]
	[SerializeField] Color laserColorDim = new Color(191, 0, 0, 255);

	[Header("Objects")]
	[SerializeField] MeshRenderer environmentLaser = null;
	[SerializeField] LineRenderer laserBeam = null;

	[Header("Particle Systems")]
	[SerializeField] ParticleSystem laserStartPoint = null;
	[SerializeField] ParticleSystem laserEmitter = null;
	[SerializeField] ParticleSystem laserReceiver = null;
	[SerializeField] ParticleSystem laserSparks = null;

	[Header("Other")]
	[SerializeField] Transform startPoint = null;
	[SerializeField] Transform endPoint = null;
	public float width = .05f;
	PlayerKnockBack playerKnockBack;
	public LayerMask IgnoreLayer;

	//[Header("SFX")]
	//[SerializeField] AudioSource laserSound = null;


	void Start()
	{
		playerKnockBack = FindObjectOfType<PlayerKnockBack>();
		if (laserIsOnAtStart)
        {
			TurnOnTheLaser();
		}
        else
        {
			TurnOffTheLaser();
		}

        if (isMovingOnStart)
        {
			StartCoroutine(laserMoveCoroutine());
		}
	}

	public void TurnOnTheLaser()
	{
		laserBeam.enabled = true;

		//laserSound.Play(0);

		var emission = laserStartPoint.emission;
		emission.enabled = true;
		
		laserBeam.startWidth = width;
		laserBeam.endWidth = width;

		environmentLaser.material.SetColor("Color_316403B1", laserColorBright);
		laserEmitter.Play();
		laserReceiver.Play();
		laserSparks.Play();
	}

	public void TurnOffTheLaser()
    {
		laserBeam.enabled = false;

		//laserSound.Stop();

		var emission = laserStartPoint.emission;
		emission.enabled = false;

		environmentLaser.material.SetColor("Color_316403B1", laserColorDim);
		laserEmitter.Stop();
		laserReceiver.Stop();
		laserSparks.Stop();
	}

	IEnumerator laserMoveCoroutine()
    {
        while (laserIsOnAtStart)
        {
			while (transform.position != waypoints[currentDestination].transform.position)
			{
				transform.position = Vector3.MoveTowards(transform.position, waypoints[currentDestination].transform.position, Time.deltaTime * laserMoveSpeed);
				yield return null;
			}

			currentDestination++;
			yield return null;

			if (waypoints.Length <= currentDestination)
			{
				currentDestination = 0;
			}

			yield return null;
		}
		yield return null;
	}

	//TODO: OPTIMIZE THIS
	void Update()
	{
		//The start of the laser beam will reflect the position of the laser pad.
		laserBeam.SetPosition(0, startPoint.position);

		//Check if the laser hit an object.
		RaycastHit hit;
		if (Physics.Raycast(transform.position, transform.forward, out hit, 1000f, ~IgnoreLayer))
		{
			if (hit.collider)
			{
				laserBeam.SetPosition(1, hit.point);
				laserReceiver.transform.position = hit.point;
				laserSparks.transform.position = hit.point;
			}
			if (hit.collider.tag == "Player" && laserBeam.enabled == true)
			{
				//Will knockback the player. //playerscript.Stagger();  //change with chirayus player controller
				playerKnockBack.LaserKnockBack();
			}
		}
		else 
		{
			laserBeam.SetPosition(1, endPoint.position);
			laserReceiver.transform.position = endPoint.position;
			laserSparks.transform.position = endPoint.position;
		}
	}
}