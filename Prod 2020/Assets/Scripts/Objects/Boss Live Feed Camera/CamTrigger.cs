﻿using System.Collections;
using UnityEngine;

public class CamTrigger : MonoBehaviour
{
    //inspector variables
    [SerializeField] LiveFeedMonitor[] sendMessageTo = null; //this variable holds the monitors to send the signal to.
    [SerializeField] float sendSignalDelay = 1f; //this variable holds the delay before the signal is sent to the monitors

    //private variables
    GameObject playerCharacter; //this variable stores the player character for easy referral

    void OnTriggerEnter(Collider other)
    {
        TriggerCheck(other);
    }

    void OnTriggerExit(Collider other)
    {
        TriggerCheck(other);
    }

    //this function is called whenever the player gets in or out of the trigger's collision shape
    void TriggerCheck(Collider other)
    {
        //checks if the player has entered before by checking if the "playerCharacter" variable is empty
        if (playerCharacter == null && other.gameObject.CompareTag("Player"))
        {
            //stores the player character
            playerCharacter = other.gameObject;
            StartCoroutine("SendSignalToMonitors");
        }

        //this goes through if the player has entered the trigger before
        else if (other.gameObject.CompareTag("Player"))
        {
            playerCharacter = null;
            StartCoroutine("SendSignalToMonitors");
        }
    }

    //this coroutine will send signals to every object inside the "sendMessageTo" variable so that the screens on those objects will change
    IEnumerator SendSignalToMonitors()
    {
        //loops through every game object inside the "sendMessageTo" variable
        //Changes the screen to static as a transition
        for (int monitors = 0; monitors < sendMessageTo.Length; monitors++)
        {
            sendMessageTo[monitors].GetComponent<LiveFeedMonitor>().StaticEffect();
        }

        //this yield function will make it sure that the signal will only go through after a specified period.
        //this is so that there can be a delay whenever the player enters or leave the room to make the screen change more organic
        yield return new WaitForSeconds(sendSignalDelay);

        //loops through every game object inside the "sendMessageTo" variable
        for(int monitors = 0; monitors < sendMessageTo.Length; monitors++)
        {
            sendMessageTo[monitors].GetComponent<LiveFeedMonitor>().ChangeScreen();
        }
    }
}
