﻿using UnityEngine;

public class LiveFeedMonitor : MonoBehaviour
{
    //Inspector Variables
    [Header("Bools")]
    [SerializeField] bool bossIsRandom = true; //variable holds if the monitor screens are randomized or not
    [SerializeField] bool adIsRandom = true;

    [Header("Random Materials")]
    [SerializeField] Material[] randomBossFootage = null; //this Material array holds the footage that's going to be used
    [SerializeField] Material[] randomAdFootage = null;

    [Header("Default Material")]
    [SerializeField] Material bossFootage = null; //same as above but only holds one material
    [SerializeField] Material normalFootage = null; //this material is what the monitor will revert back to when the player exits the trigger

    [Header("Other Material")]
    [SerializeField] Material staticMaterial = null;

    [Header("Sounds")]
    [SerializeField] AudioSource monitorStaticSound = null;

    //[SerializeField] float screenChangeDelay = 0f; //unused for now

    //Private Variables
    bool isScreenChanged = false; //checks if the screen has been changed already from the player entering the trigger
    //MeshRenderer meshrndr; //holds the screen's mesh renderer for make its material easier to refer

    void Start()
    {
        if (adIsRandom)
        {
            //changes the current material to what the "chooseFootage" variable got and picks it in the "randomBossFootage" array 
            GetComponent<MeshRenderer>().material = randomAdFootage[Random.Range(0, randomAdFootage.Length)];
        }
        else
        {
            //reverts the material back to its starting material when the scene was loaded
            GetComponent<MeshRenderer>().material = normalFootage;
        }
    }

    public void StaticEffect()
    {
        GetComponent<MeshRenderer>().material = staticMaterial;
        //play sound static when the screen is changed to this
        monitorStaticSound.Play();
    }

    //this function will change the monitors current material to the desired material
    public void ChangeScreen()
    {
        //checks of the screen has been changed
        if (!isScreenChanged)
        {
            //this will go through if this object's "isScreenRandomized" boolean is ticked
            if (bossIsRandom)
            {
                //changes the current material to what the "chooseFootage" variable got and picks it in the "randomBossFootage" array 
                GetComponent<MeshRenderer>().material = randomBossFootage[Random.Range(0, randomBossFootage.Length)];
            }

            else
            {
                //changes the object's current material to a specific material
                GetComponent<MeshRenderer>().material = bossFootage;
            }

            //declares that the screen/material has been changed
            isScreenChanged = true;
        }

        else
        {
            if (adIsRandom)
            {
                //changes the current material to what the "chooseFootage" variable got and picks it in the "randomBossFootage" array 
                GetComponent<MeshRenderer>().material = randomAdFootage[Random.Range(0, randomAdFootage.Length)];
            }
            else
            {
                //reverts the material back to its starting material when the scene was loaded
                GetComponent<MeshRenderer>().material = normalFootage;
            }

            //declares the screen/material has been reverted back to the default material
            isScreenChanged = false;
        }

        //stops the static sound
        monitorStaticSound.Stop();
    }
}
