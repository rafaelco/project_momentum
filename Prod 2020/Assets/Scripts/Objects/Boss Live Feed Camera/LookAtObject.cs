﻿using UnityEngine;

public class LookAtObject : MonoBehaviour
{
    [SerializeField] Transform target = null;

    void LateUpdate()
    {
        transform.LookAt(target, Vector3.up);
    }
}