﻿using EZCameraShake;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class EndGame : MonoBehaviour
{
    [SerializeField] EndCredits endCredits = null;
    [SerializeField] float timeUntilCutscene3 = 6f;
    [SerializeField] float timeUntilEndCredits = 12;
    [SerializeField] float forceFOV = 50f;

    [Header("SFX")]
    public AudioSource bg = null;
    [SerializeField] GameObject normalBG = null;

    [Header("Other")]
    [SerializeField] GameObject levelObject = null;
    [SerializeField] GameObject cutscene2 = null;
    [SerializeField] Image blackImage = null;
    [SerializeField] GameObject cutscene3 = null;

    [Header("Camera Shake")]
    [SerializeField] float timeBeforeShake = 2.46f;
    [SerializeField] float shakerMagnitude = 4f;
    [SerializeField] float shakerRoughness = 4f;
    [SerializeField] float shakerFadeInTime = 0.1f;
    [SerializeField] float shakerFadeOutTime = 2f;

    void Start()
    {
        //Make sure cutscene2 are not enabled
        cutscene2.SetActive(false);
        cutscene3.SetActive(false);

        //Turn Off Black Screen
        blackImage.GetComponent<Image>().enabled = false;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (!GameManager.instance.gameIsOver)
        {
            //Sound
            //Turn Off Music
            bg.gameObject.SetActive(false);

            //Turns off the camera shaker
            GameManager.instance.player.cameraShaker.enabled = false;

            //Change Boss Screen to static
            endCredits.creditScreen.material = endCredits.staticMaterial;

            StartCoroutine(FrameDelay());
        }
    }

    IEnumerator FrameDelay()
    {
        yield return null;

        //Mark Game As Finished
        GameManager.instance.gameIsOver = true;

        //Turn Off Level and Turn On Cutscene 2
        levelObject.SetActive(false);
        cutscene2.SetActive(true);
        cutscene3.SetActive(false);

        //Enables Black
        blackImage.GetComponent<Image>().enabled = true;

        //Start Timer Until Shake Effect
        StartCoroutine(ShakeTimer());

        //Start Timer Until Cutscene 3
        StartCoroutine(Cutscene3Timer());

        //Start Timer Until Credits Kick In
        StartCoroutine(CreditsTimer());
    }

    IEnumerator ShakeTimer()
    {
        yield return new WaitForSeconds(timeBeforeShake);

        CameraShaker.Instance.ShakeOnce(shakerMagnitude, shakerRoughness, shakerFadeInTime, shakerFadeOutTime);
    }

    IEnumerator Cutscene3Timer()
    {
        yield return new WaitForSeconds(timeUntilCutscene3);

        //Turn On Level and Turn Off Cutscenes
        cutscene2.SetActive(false);
        cutscene3.SetActive(true);
    }

    IEnumerator CreditsTimer()
    {
        yield return new WaitForSeconds(timeUntilEndCredits);

        //Turn On Level and Turn Off Cutscenes
        levelObject.SetActive(true);
        cutscene2.SetActive(false);
        cutscene3.SetActive(false);

        //Disable Normal Music
        normalBG.SetActive(false);

        yield return null;

        //Move Camera to End Credit Position
        StartCoroutine(endCredits.MoveCameraToPosition());

        //Fade Out
        StartCoroutine(GameManager.instance.EndGameFadeInOut());

        //Force Camera FOV
        GameManager.instance.player.cam.fieldOfView = forceFOV;
    }
}