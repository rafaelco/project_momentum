﻿using DG.Tweening;
using System.Collections;
using UnityEngine;

public class PistonBehavior : MonoBehaviour
{
    #region Interface Variables

    [TextArea(minLines: 3,maxLines: 6, order = 0)]
    public string Instructions = "Follow the tooltips on each property to know exactly what each part do." +
        "It is important to follow these guidelines so that the whole thing won't break." +
        "PLEASE. Do not change the scale of the piston prefab itself. If you want to modify" +
        "the size of the piston, do it directly on the object itself";
  
    [Header("Quick Tip: Hover on a property to show its tooltip", order = 1)]

    [Header("Piston Function", order = 2)]
    [Tooltip("Ticking on this property will turn on the piston when the game is run")]
    [SerializeField] bool isActivatedFromStart = false;
    [Tooltip("Piston won't start its function until this delay has ended")]
    [SerializeField] float startDelay = 0f;
    [Tooltip("Ticking on this property will make the piston crush the player if the piston catches them")]
    [SerializeField] bool pistonIsCrusher = false;
    [Tooltip("Ticking on this property will make the piston push objects or the player")]
    [SerializeField] bool pistonIsLauncher = false;

    [Header("Piston Parts", order = 3)]
    [Tooltip("Do Not Touch. This property holds the 'crank' of the piston so that it can crush or push objects")]
    [SerializeField] GameObject pistonCrankPivot = null;

    [Header("Collision Boxes", order = 4)]
    [Tooltip("Do Not Touch. This property holds the collision box for the piston head")]
    [SerializeField] Collider pistonCollider = null;

    [Header("Piston Direction", order = 5)]
    [Tooltip("This property holds the coordinates where the piston will start before it drops. Place an empty Game Object to serve as a waypoint")]
    [SerializeField] GameObject pistonStart = null;
    [Tooltip("This property holds the coordinates where the piston will stop before it goes back up. Place an empty Game Object to serve as a waypoint")]
    [SerializeField] GameObject pistonDestination = null;

    [Header("Piston Attributes", order = 6)]
    [Tooltip("This propety controls if the piston works or not")]
    [SerializeField] bool isWorking = true;
    [Tooltip("Ticking on this property will make sure the piston shakes first before it goes to the end destination")]
    [SerializeField] bool shakesBeforeFalling = false;
    [Tooltip("This property stores how long the piston will shake before starts going to the destination. In seconds")]
    [SerializeField] float shakeTimerBeforeFall = 0f;
    [Tooltip("This property stores how long the piston will take to reach the end waypoint. In seconds")]
    [SerializeField] float timeToReachDestination = 0f;
    [Tooltip("This property stores how long the pistol will take to reach the start waypoint after reaching the destination. In seconds")]
    [SerializeField] float timeToRetractToOrigin = 0f;
    [Tooltip("This property stores how long the piston will stay at the destination before it starts retracting towards the start waypoint")]
    [SerializeField] float timeStoppedAtDestination = 0f;
    [Tooltip("This property stores how long the pistol will stay in place at the start waypoint before it starts going to the destination")]
    [SerializeField] float timeStoppedAtStart = 0f;
    [Tooltip("This property stores easing functions used by the piston when going towards the destination. Refer 'easings.net' to learn about each option")]
    [SerializeField] Ease PullEaseFormula = Ease.OutSine;
    [Tooltip("This property stores easing functions used by the piston when retracting towards the start. Refer 'easings.net' to learn about each option")]
    [SerializeField] Ease PushEaseFormula = Ease.OutBounce;

    [Header("Piston Launcher Attributes", order = 7)]
    [Tooltip("This property stores how strong the push will be when it hits the player. 6-digits or higher recommended for visible impact")]
    [SerializeField] float pushForce = 0f;

    [Header("Sounds")]
    [SerializeField] AudioSource pistonShakeSound = null;
    [SerializeField] AudioSource pistonExtendSound = null;
    [SerializeField] AudioSource pistonRetractSound = null;

    #endregion

    #region Private Variables
    IEnumerator pistonCoroutine; //contains the coroutine so that it can easily be accessed
    Tween pistonTween; //this variable contains the tween used to move the piston's crank and head
    bool isLaunching; // this variable will make sure that the piston will only launch the player if the pushing tween is happening
    MeshCollider collisionBox; //to easily get the piston head's collisionbox
    Collision savedCollider; //grabs the player whenever the player collides with the piston [used for crusher or launchers]
    GameObject playerCharacter; //the player game object is stored in this variable
    #endregion

   
    void Start()
    {
        //check if the "isActivatedFromStart" boolean is ticked in the inspector when the game is run
        if (isActivatedFromStart)
        {
            StartCoroutine(PistonTimer());
        }

        //grabs the piston head's collision box for modifications
        collisionBox = pistonCollider.GetComponent<MeshCollider>();
    }

    IEnumerator PistonTimer()
    {
        float time = 0f;
        while (time < startDelay)
        {
            time += Time.deltaTime;
            yield return null;
        }
        StartPistonFunction();
    }

    //is called to start the piston's function
    public void StartPistonFunction()
    {
        pistonCoroutine = PistonMovement();
        StartCoroutine(pistonCoroutine);
    }

    //is called to stop the pistons from moving
    public void StopPistonFunction()
    {
        StopCoroutine(pistonCoroutine);
    }

    //the entire piston function is inside this coroutine
    IEnumerator PistonMovement()
    {
        while (isWorking)
        {
            //the piston will not drop immediately when spawned or the loop restarts
            yield return new WaitForSeconds(timeStoppedAtStart);

            //this shake is to tell the player when the piston is about to fall so they can react to it
            if (shakesBeforeFalling)
            {
                //shakes the piston head in place
                pistonTween = pistonCrankPivot.transform.DOShakePosition(shakeTimerBeforeFall, 0.05f, 10, 90);
                //this is to make sure this coroutine will not do anything else until the tween has completed its task
                yield return pistonTween.WaitForCompletion();
                //play sound
                pistonShakeSound.Play();
            }

            //play sound
            pistonShakeSound.Stop();
            pistonExtendSound.Play();

            //the piston falls down quickly
            pistonTween = pistonCrankPivot.transform.DOMove(pistonDestination.transform.position, timeToReachDestination).SetEase(PushEaseFormula);

            //enables the collision box for piston head
            if (pistonIsCrusher)
            {
                //Collision will be enabled when the piston is halfway from its destination
                yield return pistonTween.WaitForPosition(timeToReachDestination * .5f);
                pistonCollider.enabled = true;
            }
            //Collision will be enabled when the piston starts going to its destination
            //will also start pushing the player if the player is within range
            else if (pistonIsLauncher)
            {
                //so that the player can be launched whenever the piston is pushing
                isLaunching = true;

                //this will push the player even if the collision of the player and piston was registered earlier
                if (savedCollider != null)
                    OnCollisionEnter(savedCollider);
            }

            //this is to make sure this coroutine will not do anything else until the tween has completed its task
            yield return pistonTween.WaitForCompletion();

            //disables the collision box for piston head
            if (pistonIsCrusher)
            {
                //Collision will be enabled when the piston is halfway from its destination
                pistonCollider.enabled = false;
            }
            else if (pistonIsLauncher)
            {
                //"grace period" where the player can still be pushed even if they actually didn't reach the piston on time before it stops pushing
                yield return new WaitForSeconds(0.1f);

                //stops the piston from launching the player so that the player can stand on it
                isLaunching = false;
            }

            //makes sure that the piston stay in place after it has crush the player
            if (!isWorking)
                yield break;

            //this pause is how long the piston will stay in its current position
            yield return new WaitForSeconds(timeStoppedAtDestination);

            //play sounds
            pistonExtendSound.Stop();
            pistonRetractSound.Play();

            //the piston goes back to its starting position
            pistonTween = pistonCrankPivot.transform.DOMove(pistonStart.transform.position, timeToRetractToOrigin).SetEase(PullEaseFormula);

            //this is to make sure this coroutine will not do anything else until the tween has completed its task
            yield return pistonTween.WaitForCompletion();
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        CollisionCheck(collision);
    }

    void OnCollisionExit(Collision collision)
    {
        if(savedCollider != null)
        {
            savedCollider = null;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (!pistonIsLauncher || !pistonIsCrusher)
            {
                playerCharacter = other.gameObject;
                updateParentingOnPlayerCharacter(other.gameObject);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (!pistonIsLauncher || !pistonIsCrusher)
            {
                updateParentingOnPlayerCharacter(other.gameObject);
            }
        }
    }

    //is called when a collision happens
    void CollisionCheck(Collision collision)
    {
        //Check if the collision was with the Piston head
        if (collision.GetContact(0).otherCollider.CompareTag("Player"))
        {
            //saves the player's collision so that when the piston pushes again, it'll also push the player
            savedCollider = collision;

            //Check if the collider has a tag of "Player"
            if (collision.GetContact(0).thisCollider.GetComponent<Collider>() == pistonCollider)
            {
                //Crush Player
                if (pistonIsCrusher)
                    CrushPlayer();

                //Launch Player/Object
                else if (pistonIsLauncher && isLaunching)
                    LaunchPlayer(collision);
            }
        }
    }

    //is called when the player is caught by the piston and it's set on "Launcher"
    void LaunchPlayer(Collision objectToPush)
    {
        GameObject pushableObject = objectToPush.gameObject;
        Rigidbody rb = pushableObject.GetComponent<Rigidbody>();

        //pushes the object/player in the direction opposite of the piston's green arrow/axis
        rb.AddForce(-transform.up * pushForce);
    }

    //is called when the player is caught by the piston and it's set on "Crusher"
    void CrushPlayer()
    {
        GameManager.instance.killedByCrusher = true;
        GameManager.instance.RespawnPlayer();
    }

    //parents and unparents the player from the piston
    void updateParentingOnPlayerCharacter(GameObject player)
    {
        if (playerCharacter != null)
            player.transform.SetParent(pistonCrankPivot.transform, true);
        else
            player.transform.SetParent(null, true);
            playerCharacter = null;

    }
}