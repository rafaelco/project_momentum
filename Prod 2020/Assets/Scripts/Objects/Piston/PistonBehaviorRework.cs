﻿using Cinemachine;
using DG.Tweening;
using System.Collections;
using UnityEngine;

public class PistonBehaviorRework : MonoBehaviour
{
    #region Interface Variables

    [TextArea(minLines: 3, maxLines: 6, order = 0)]
    public string Instructions = "Follow the tooltips on each property to know exactly what each part do." +
        "It is important to follow these guidelines so that the whole thing won't break." +
        "PLEASE. Do not change the scale of the piston prefab itself. If you want to modify" +
        "the size of the piston, do it directly on the object itself";

    [Header("Quick Tip: Hover on a property to show its tooltip", order = 1)]

    [Header("Piston Parts", order = 2)]
    [Tooltip("Do Not Touch. This property holds the 'crank' of the piston so that it can crush or push objects")]
    [SerializeField] GameObject pistonCrankPivot = null;
    [Tooltip("Do Not Touch. This property holds the trajectory used to launch the player")]
    [SerializeField] CinemachineSmoothPath trajectory = null;

    [Header("Piston Direction", order = 4)]
    [Tooltip("This property holds the coordinates where the piston will start before it drops. Place an empty Game Object to serve as a waypoint")]
    [SerializeField] GameObject pistonStart = null;
    [Tooltip("This property holds the coordinates where the piston will stop before it goes back up. Place an empty Game Object to serve as a waypoint")]
    [SerializeField] GameObject pistonDestination = null;

    [Header("Piston Attributes", order = 5)]
    [Tooltip("This propety controls if the piston works or not")]
    [SerializeField] bool isWorking = true;
    [Tooltip("Ticking on this property will make sure the piston shakes first before it goes to the end destination")]
    [SerializeField] bool shakesBeforeFalling = false;
    [Tooltip("This property stores how long the piston will shake before starts going to the destination. In seconds")]
    [SerializeField] float shakeTimerBeforeFall = 0f;
    [Tooltip("This property stores how long the piston will take to reach the end waypoint. In seconds")]
    [SerializeField] float timeToReachDestination = 0f;
    [Tooltip("This property stores how long the pistol will take to reach the start waypoint after reaching the destination. In seconds")]
    [SerializeField] float timeToRetractToOrigin = 0f;
    [Tooltip("This property stores how long the piston will stay at the destination before it starts retracting towards the start waypoint")]
    [SerializeField] float timeStoppedAtDestination = 0f;
    [Tooltip("This property stores how long the pistol will stay in place at the start waypoint before it starts going to the destination")]
    [SerializeField] float timeStoppedAtStart = 0f;
    [Tooltip("This property stores easing functions used by the piston when going towards the destination. Refer 'easings.net' to learn about each option")]
    [SerializeField] Ease PullEaseFormula = Ease.OutSine;
    [Tooltip("This property stores easing functions used by the piston when retracting towards the start. Refer 'easings.net' to learn about each option")]
    [SerializeField] Ease PushEaseFormula = Ease.OutBounce;

    [Header("Jump Parameters", order = 6)]
    [Tooltip("How long it'll take for the player to reach the destination while leaping")]
    [SerializeField] float jumpTime = 0.0f;
    [Tooltip("How will the player leap. Refer to easings.net to see what each easing method does")]
    [SerializeField] Ease easingMethod = Ease.Linear;

    [Header("Jump Effects", order = 7)]
    [SerializeField] bool changeFOVOnJump = true;
    [SerializeField] float jumpFOV = 0.0f;

    [Header("SFX", order = 8)]
    [SerializeField] AudioSource pistonShakeSound = null;
    [SerializeField] AudioSource pistonExtendSound = null;
    [SerializeField] AudioSource pistonRetractSound = null;

    #endregion

    #region Private Variables
    IEnumerator pistonCoroutine; //contains the coroutine so that it can easily be accessed
    Tween pistonTween; //this variable contains the tween used to move the piston's crank and head
    bool isLaunching; // this variable will make sure that the piston will only launch the player if the pushing tween is happening
    //bool characterIsJumping; //this variable is used to check if the player has used the ramp or not before it can call a jump again to avoid bugs and glitches while leaping 
    Collision savedCollider; //grabs the player whenever the player collides with the piston [used for crusher or launchers]
    public GameObject playerCharacter; //the player game object is stored in this variable
    CharacterAnimationController characterAnimationController;
    #endregion

    private void Start()
    {
        characterAnimationController = FindObjectOfType<CharacterAnimationController>();
    }

    //Is called to start the piston's function
    public void StartPistonFunction()
    {
        pistonCoroutine = PistonMovement();

        StartCoroutine(pistonCoroutine);
    }

    //The entire piston function is inside this coroutine
    IEnumerator PistonMovement()
    {
        //the piston will not drop immediately when spawned or the loop restarts
        yield return new WaitForSeconds(timeStoppedAtStart);

        //this shake is to tell the player when the piston is about to fall so they can react to it
        if (shakesBeforeFalling)
        {
            //play sound
            pistonShakeSound.Play();
            //shakes the piston head in place
            pistonTween = pistonCrankPivot.transform.DOShakePosition(shakeTimerBeforeFall, 0.1f, 10, 90);
            //this is to make sure this coroutine will not do anything else until the tween has completed its task
            yield return pistonTween.WaitForCompletion();
        }

        //play sound
        pistonShakeSound.Stop();
        pistonExtendSound.Play();

        //The piston moves towards destination
        pistonTween = pistonCrankPivot.transform.DOMove(pistonDestination.transform.position, timeToReachDestination).SetEase(PushEaseFormula);

        //Call Coroutine that will launch the player
        if (playerCharacter != null)
        {
            yield return pistonTween.WaitForPosition(0.1f);
            StartCoroutine(LaunchPlayer());
        }

        //this is to make sure this coroutine will not do anything else until the tween has completed its task
        yield return pistonTween.WaitForCompletion();

        //makes sure that the piston stay in place after it has crush the player
        if (!isWorking)
            yield break;

        //this pause is how long the piston will stay in its current position
        yield return new WaitForSeconds(timeStoppedAtDestination);

        //play sounds
        pistonExtendSound.Stop();
        pistonRetractSound.Play();

        //the piston goes back to its starting position
        pistonTween = pistonCrankPivot.transform.DOMove(pistonStart.transform.position, timeToRetractToOrigin).SetEase(PullEaseFormula);

        //this is to make sure this coroutine will not do anything else until the tween has completed its task
        yield return pistonTween.WaitForCompletion();
    }

    IEnumerator LaunchPlayer()
    {
        StartCoroutine(PlayerForceAnimation());
        //This changes the speed that the camera travels.
        GameManager.instance.player.keepCameraWithPlayer.camSpeed = GameManager.instance.player.cameraSpeedSlow;

        //Note: If we want the jumps to be more realistic. We'll calculate the "jumpTime" with T = Distance / Player's Velocity instead of putting in the inspector

        //yield return new WaitForSeconds(0.1f); add this if jump does not feel realistic enough

        //Takes controls from the player
        GameManager.instance.player.playerInput.enabled = false;
        GameManager.instance.player.characterMovement.playerFollowingTrajectory = true;

        //Setup player character's Cinemachine Dolly Cart
        CinemachineDollyCart playerDollyCart = playerCharacter.AddComponent<CinemachineDollyCart>();

        playerDollyCart.m_Path = trajectory;
        playerDollyCart.m_UpdateMethod = CinemachineDollyCart.UpdateMethod.Update;
        playerDollyCart.m_PositionUnits = CinemachinePathBase.PositionUnits.Normalized;

        //Adjust starting jump waypoint so that the jump feels more natural
        trajectory.m_Waypoints[0].position = transform.InverseTransformPoint(playerCharacter.transform.position);
  
        //This code will move the waypoints on the position (the local x specifically) of the player during the jump.
        float testFloat = trajectory.m_Waypoints[0].position.x;
        for (int i = 0; i < trajectory.m_Waypoints.Length; i++)
        {
            trajectory.m_Waypoints[i].position.x = testFloat;
        }

        //This will refresh the resolution of the jump path.
        trajectory.m_Resolution = trajectory.m_Resolution + 1;

        //Rotates the Player
        GameManager.instance.player.characterMovement.t_mesh.rotation = Quaternion.identity;
        StartCoroutine(GameManager.instance.player.characterMovement.RampRotation(gameObject));

        //Changes the Player FOV
        if (changeFOVOnJump)
            GameManager.instance.player.destinationFOV = jumpFOV;
            GameManager.instance.player.StartFOVCoroutine();

        //Make the player leap
        yield return DOTween.To(x => playerDollyCart.m_Position = x, 0f, 1f, jumpTime).SetEase(easingMethod).WaitForCompletion();
        yield return null;

        //After Leap Setup:
        //Removes the player's Dolly Cart component so that the player can use a ramp again without any hiccups
        Destroy(playerDollyCart);

        //Hack fix. Please don't keep this. Find a way for the player's rotation to not get affected during leaps
        playerCharacter.transform.rotation = Quaternion.Euler(Vector3.zero); 
        GameManager.instance.player.characterMovement.t_mesh.rotation = Quaternion.Euler(Vector3.zero);

        //Gives Controls back to the player
        GameManager.instance.player.playerInput.enabled = true;
        GameManager.instance.player.characterMovement.playerFollowingTrajectory = false;
        characterAnimationController.animator.SetBool("fallingFromRamp", false);

        //Returns the player FOV back to normal
        if (changeFOVOnJump)
            GameManager.instance.player.destinationFOV = GameManager.instance.player.normalFOV;
            GameManager.instance.player.StartFOVCoroutine();

        yield return new WaitForSeconds(0.5f);

        //This changes the speed that the camera travels.
        GameManager.instance.player.keepCameraWithPlayer.camSpeed = GameManager.instance.player.cameraSpeedNormal;

        //Unassigns player from Piston which allows them to use it again.
        playerCharacter = null;
    }

    void OnTriggerEnter(Collider other)
    {
        if (playerCharacter == null && other.gameObject.CompareTag("Player"))
        {
            playerCharacter = other.gameObject;
            StartPistonFunction();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && GameManager.instance.player.playerInput.enabled == true)
        {
            playerCharacter = null;
        }
    }

    IEnumerator PlayerForceAnimation()
    {
        yield return new WaitForSeconds(0.1f);
        characterAnimationController.animator.SetBool("fallingFromRamp", true);
    }
}