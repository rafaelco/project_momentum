﻿using UnityEngine;

public class Trigger_Anticipation : MonoBehaviour
{
    CharacterAnimationController characterAnimationController;

    void Start()
    {
        characterAnimationController = GameManager.instance.player.gameObject.GetComponent<CharacterAnimationController>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            if(GameManager.instance.player.currentMomentum != ChargeMode.NONE)
            {
                characterAnimationController.animator.SetBool("isAnticipating", true);
            }
            
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (GameManager.instance.player.currentMomentum != ChargeMode.NONE)
            {
                characterAnimationController.animator.SetBool("isAnticipating", false);
                Destroy(gameObject);
            }
        }
    }
}