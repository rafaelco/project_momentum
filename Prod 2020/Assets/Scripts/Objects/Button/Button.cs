﻿using System.Collections;
using UnityEngine;
using EZCameraShake;

public class Button : MonoBehaviour
{
    [Header("Activation Status")]
    [SerializeField] bool hasBeenActivated = false;
    [SerializeField] GameObject buttonObject = null;
    [SerializeField] Transform activationDestination = null;
    [SerializeField] float speed = 1f;
    [SerializeField] MeshRenderer meshRenderer = null;

    [Header("Gameplay Objects")]
    [SerializeField] Laser[] lasersToTurnOn = null;
    [SerializeField] Laser[] lasersToTurnOff = null;
    [SerializeField] Door[] doorsToOpen = null;
    [SerializeField] Door[] doorsToClose = null;
    [SerializeField] SpecialLight[] lightsToTurnOn = null;
    [SerializeField] SpecialLight[] lightsToTurnOff = null;
    [SerializeField] PistonBehavior[] pistonsToActivate = null;
    [SerializeField] PistonBehavior[] pistonsToDeactivate = null;
    [SerializeField] BridgeScript[] bridges = null;
    [SerializeField] RampScript[] ramps = null;
    [SerializeField] ChargeMode newRampMode = ChargeMode.HIGH;

    [Header("Camera Shake")]
    [SerializeField] float shakerMagnitude = 4f;
    [SerializeField] float shakerRoughness = 4f;
    [SerializeField] float shakerFadeInTime = 0.1f;
    [SerializeField] float shakerFadeOutTime = 2f;

    CharacterAnimationController characterAnimationController;
    CharacterMovement characterMovement;
    PlayerAbility playerAbility;

    [Header("SFX")]
    [SerializeField] AudioSource buttonSound = null; 

    void Start()
    {
        characterAnimationController = GameManager.instance.player.gameObject.GetComponent<CharacterAnimationController>();

        characterMovement = FindObjectOfType<CharacterMovement>();
        playerAbility = FindObjectOfType<PlayerAbility>();

        if (hasBeenActivated)
        {
            meshRenderer.material.SetColor("Color_316403B1", Color.black);
            buttonObject.transform.position = activationDestination.transform.position;
        }
    }

    void OnTriggerEnter(Collider player)
    {
        //This function will trigger the button while punching
        // Check if player is facing front and button is still not activated
        if (GameManager.instance.player.currentMomentum != ChargeMode.NONE)
        {
            if (player.tag == "FrontCollision" && hasBeenActivated == false)            
            {
                characterAnimationController.animator.SetBool("punch", true);

                //Play VFX here
                GameManager.instance.player.hitParticle.Play();

                StartCoroutine(WaitForAnimation());
                characterMovement.Velocity = Vector3.zero;
                playerAbility.playerInput.MakePlayerStop();
            }
        }
    }

    void OnTriggerExit(Collider player)
    {
        if (player.tag == "FrontCollision" )
        {
            characterAnimationController.animator.SetBool("punch", false);
        }
    }

    void Activate()
    {
        //Will Shake the Camera
        CameraShaker.Instance.ShakeOnce(shakerMagnitude, shakerRoughness, shakerFadeInTime, shakerFadeOutTime);

        //Lasers
        if (lasersToTurnOn.Length > 0)
        {
            for (int i = 0; i < lasersToTurnOn.Length; i++)
            {
                lasersToTurnOn[i].TurnOnTheLaser();
            }
        }
        if (lasersToTurnOff.Length > 0)
        {
            for (int i = 0; i < lasersToTurnOff.Length; i++)
            {
                lasersToTurnOff[i].TurnOffTheLaser();
            }
        }

        //Doors
        if (doorsToOpen.Length > 0)
        {
            for (int i = 0; i < doorsToOpen.Length; i++)
            {
                doorsToOpen[i].OpenTheDoor();
            }
        }
        if (doorsToClose.Length > 0)
        {
            for (int i = 0; i < doorsToClose.Length; i++)
            {
                doorsToClose[i].CloseTheDoor();
            }
        }

        //Bridges
        if (bridges.Length > 0)
        {
            for (int i = 0; i < bridges.Length; i++)
            {
                bridges[i].MoveTheBridge();
            }
        }

        //Lights
        if (lightsToTurnOn.Length > 0)
        {
            for (int i = 0; i < lightsToTurnOn.Length; i++)
            {
                lightsToTurnOn[i].TurnOnTheLight();
            }
        }
        if (lightsToTurnOff.Length > 0)
        {
            for (int i = 0; i < lightsToTurnOff.Length; i++)
            {
                lightsToTurnOff[i].TurnOffTheLight();
            }
        }

        //Pistons
        if (pistonsToActivate.Length > 0)
        {
            for (int i = 0; i < pistonsToActivate.Length; i++)
            {
                pistonsToActivate[i].StartPistonFunction();
            }
        }
        if (pistonsToDeactivate.Length > 0)
        {
            for (int i = 0; i < pistonsToDeactivate.Length; i++)
            {
                pistonsToDeactivate[i].StopPistonFunction();
            }
        }

        if(ramps.Length > 0)
        {
            for (int i = 0; i < ramps.Length; i++)
            {
                ramps[i].rampMode = newRampMode;
                ramps[i].RampModeSelection();
            }
        }
        
        DisableButton();
    }

    void DisableButton()
    {
        hasBeenActivated = true;

        StartCoroutine(MoveButton());
    }

    IEnumerator MoveButton()
    {
        meshRenderer.material.SetColor("Color_316403B1", Color.black);

        while (buttonObject.transform.position != activationDestination.transform.position)
        {
            buttonObject.transform.position = Vector3.MoveTowards(buttonObject.transform.position, activationDestination.transform.position, Time.deltaTime * speed);
            yield return null;
        }
        StopCoroutine(MoveButton());
        yield return null;
    }

    IEnumerator WaitForAnimation()
    {
        yield return new WaitForSeconds(0.3f);
        buttonSound.Play(0);
        Activate();
    }
}