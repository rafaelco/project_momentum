﻿using UnityEngine;

public class RampCollider : MonoBehaviour
{
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.collider.GetComponent<CharacterMovement>().playerFollowingTrajectory = true;
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.collider.GetComponent<CharacterMovement>().playerFollowingTrajectory = false;
        }
    }
}