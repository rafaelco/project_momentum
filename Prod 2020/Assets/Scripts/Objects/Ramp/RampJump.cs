﻿using Cinemachine;
using DG.Tweening;
using System.Collections;
using UnityEngine;

public class RampJump : MonoBehaviour
{
    #region Inspector Variables
    [Header("Tip: Hover a variable to see its tooltip", order = 0)]

    [Header("Scripts Needed", order = 1)]
    [Tooltip("This script holds the waypoints the player will use to leap")]
    [SerializeField] CinemachineSmoothPath JumpPath = null;

    [Header("Jump Parameters", order = 2)]
    [Tooltip("How long it'll take for the player to reach the destination while leaping")]
    [SerializeField] float jumpTime = 0.0f;
    [Tooltip("How will the player leap. Refer to easings.net to see what each easing method does")]
    [SerializeField] Ease easingMethod = Ease.Linear;

    [Header("Jump Effects", order = 3)]
    [SerializeField] bool changeFOVOnJump = true;
    [SerializeField] float jumpFOV = 0.0f;

    [Header("Time Manipulation Parameters", order = 4)]
    [SerializeField] bool rampIsSlowMo = false;
    [SerializeField] float timeSlowMo = 0.5f;
    #endregion

    #region Private Variables
    private Character character;
    GameObject playerCharacter;
    IEnumerator jumpCoroutine;
    CharacterAnimationController characterAnimationController;
    bool characterIsJumping; //this variable is used to check if the player has used the ramp or not before it can call a jump again to avoid bugs and glitches while leaping 
    #endregion

    private void Start()
    {
        characterAnimationController = FindObjectOfType<CharacterAnimationController>();
        character = FindObjectOfType<Character>();
    }
    [Header("SFX")]
    [SerializeField] AudioSource rampSound = null;

    void OnTriggerEnter(Collider other)
    {
        //check if the one who started the trigger is the player AND if the player hasn't used the ramp yet before
        if (other.gameObject.CompareTag("Player") && !characterIsJumping)
        {
            if(GameManager.instance.player.currentMomentum == ChargeMode.HIGH)
            {
                playerCharacter = other.gameObject;
                //character.Jump();

                rampSound.Play(0);

                characterIsJumping = true;

                jumpCoroutine = PlayerUsedRamp();
                StartCoroutine(jumpCoroutine);

                //Test
                if (rampIsSlowMo)
                {
                    rampSound.Play(0);
                    StartCoroutine(RampSlowMo());
                }
            }
        }
    }

    IEnumerator PlayerUsedRamp()
    {
        StartCoroutine(PlayerForceAnimation());
        //This changes the speed that the camera travels.
        GameManager.instance.player.keepCameraWithPlayer.camSpeed = GameManager.instance.player.cameraSpeedSlow;

        //Note: If we want the jumps to be more realistic. We'll calculate the "jumpTime" with T = Distance / Player's Velocity instead of putting in the inspector

        //yield return new WaitForSeconds(0.1f); add this if jump does not feel realistic enough

        //Takes controls from the player
        GameManager.instance.player.playerInput.enabled = false;
        GameManager.instance.player.characterMovement.playerFollowingTrajectory = true;

        //Setup player character's Cinemachine Dolly Cart
        CinemachineDollyCart playerDollyCart = playerCharacter.AddComponent<CinemachineDollyCart>();

        playerDollyCart.m_Path = JumpPath;
        playerDollyCart.m_UpdateMethod = CinemachineDollyCart.UpdateMethod.Update;
        playerDollyCart.m_PositionUnits = CinemachinePathBase.PositionUnits.Normalized;

        //Adjust starting jump waypoint so that the jump feels more natural
        JumpPath.m_Waypoints[0].position = transform.InverseTransformPoint(playerCharacter.transform.position);

        //This code will move the waypoints on the position (the local x specifically) of the player during the jump.
        float testFloat = JumpPath.m_Waypoints[0].position.x;
        for (int i = 0; i < JumpPath.m_Waypoints.Length; i++)
        {
            JumpPath.m_Waypoints[i].position.x = testFloat;
        }

        //This will refresh the resolution of the jump path.
        JumpPath.m_Resolution = JumpPath.m_Resolution + 1;

        //Rotates the Player
        GameManager.instance.player.characterMovement.t_mesh.rotation = Quaternion.identity;
        StartCoroutine(GameManager.instance.player.characterMovement.RampRotation(gameObject));

        //Changes the Player FOV
        if (jumpFOV > 110f) //110 is the player ability's High Momentum FOV
        {
            if (changeFOVOnJump)
                GameManager.instance.player.destinationFOV = jumpFOV;
                GameManager.instance.player.StartFOVCoroutine();
        }

        //Make the player leap
        yield return DOTween.To(x => playerDollyCart.m_Position = x, 0f, 1f, jumpTime).SetEase(easingMethod).WaitForCompletion();
        yield return null;

        //After Leap Setup:
        //Removes the player's Dolly Cart component so that the player can use a ramp again without any hiccups
        Destroy(playerDollyCart);

        //Ramp now lets the player activate the "PlayerUsedRamp" again
        characterIsJumping = false;

        //Hack fix. Please don't keep this. Find a way for the player's rotation to not get affected during leaps
        playerCharacter.transform.rotation = Quaternion.Euler(Vector3.zero);
        GameManager.instance.player.characterMovement.t_mesh.rotation = Quaternion.Euler(Vector3.zero);

        //Gives Controls back to the player
        GameManager.instance.player.playerInput.enabled = true;
        GameManager.instance.player.characterMovement.playerFollowingTrajectory = false;
        characterAnimationController.animator.SetBool("fallingFromRamp", false);

        //Returns the player FOV back to normal
        if (jumpFOV > 110f) //110 is the player ability's High Momentum FOV
        {
            if (changeFOVOnJump)
                GameManager.instance.player.destinationFOV = 110f;
                GameManager.instance.player.StartFOVCoroutine();
        }


        //This will reset the resolution of the jump path.
        JumpPath.m_Resolution = JumpPath.m_Resolution - 1;

        yield return new WaitForSeconds(0.5f);

        //This changes the speed that the camera travels.
        GameManager.instance.player.keepCameraWithPlayer.camSpeed = GameManager.instance.player.cameraSpeedNormal;
    }

    IEnumerator RampSlowMo()
    {
        GameManager.instance.player.SlowMo();

        yield return new WaitForSeconds(timeSlowMo);

        GameManager.instance.player.NoMo(true);
    }

    IEnumerator PlayerForceAnimation()
    {
        yield return new WaitForSeconds(0.1f);
        characterAnimationController.animator.SetBool("fallingFromRamp", true);
    }
}