﻿using UnityEngine;

public class RampScript : MonoBehaviour
{
    public ChargeMode rampMode;

    public GameObject rampMesh = null;

    [Header("Colors")]
    [ColorUsage(true, true)]
    [SerializeField] Color colorDefault = new Color(191, 191, 191, 255);
    [ColorUsage(true, true)]
    [SerializeField] Color colorLow = new Color(0, 150, 255, 255);
    [ColorUsage(true, true)]
    [SerializeField] Color colorMedium = new Color(181, 0, 255, 255);
    [ColorUsage(true, true)]
    [SerializeField] Color colorHigh = new Color(255, 168, 0, 255);

    void Start()
    {
        RampModeSelection();
    }

    public void RampModeSelection()
    {
        switch (rampMode)
        {
            case ChargeMode.LOW:
                rampMesh.GetComponent<Renderer>().material.SetColor("Color_316403B1", colorLow);
                break;
            case ChargeMode.MEDIUM:
                rampMesh.GetComponent<Renderer>().material.SetColor("Color_316403B1", colorMedium);
                break;
            case ChargeMode.HIGH:
                rampMesh.GetComponent<Renderer>().material.SetColor("Color_316403B1", colorHigh);
                break;
            default:
                rampMesh.GetComponent<Renderer>().material.SetColor("Color_316403B1", colorDefault);
                break;
        }
    }
}