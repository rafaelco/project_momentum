﻿using System.Collections;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] GameObject door = null;
    [SerializeField] bool DoorIsOpenOnStart = true;
    [SerializeField] float speed = 1;
    [SerializeField] Transform currentDestination = null;
    IEnumerator doorCoroutine;

    [Header("Positions")]
    [SerializeField] Transform positionClosed = null;
    [SerializeField] Transform positionOpen = null;

    [Header("SFX")]
    [SerializeField] AudioSource doorMovingSound = null;
    [SerializeField] AudioSource doorClosedSound = null;


    void Start()
    {
        if (DoorIsOpenOnStart)
        {
            door.transform.position = positionOpen.transform.position;
        }
        else
        {
            door.transform.position = positionClosed.transform.position;
        }
    }

    public void OpenTheDoor()
    {
        if (doorCoroutine != null)
        {
            StopCoroutine(doorCoroutine);
        }

        currentDestination = positionOpen;

        doorCoroutine = MoveDoor();
        StartCoroutine(doorCoroutine);
    }

    public void CloseTheDoor()
    {
        if (doorCoroutine != null)
        {
            StopCoroutine(doorCoroutine);
        }

        currentDestination = positionClosed;

        doorCoroutine = MoveDoor();
        StartCoroutine(doorCoroutine);
    }

    IEnumerator MoveDoor()
    {
        doorMovingSound.Play(0);

        while (door.transform.position != currentDestination.transform.position)
        {
            door.transform.position = Vector3.MoveTowards(door.transform.position, currentDestination.transform.position, Time.deltaTime * speed);
            yield return null;
        }

        doorMovingSound.Stop();
        doorClosedSound.Play();
        StopCoroutine(doorCoroutine);
        yield return null;
    }
}