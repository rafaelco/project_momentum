﻿using UnityEngine;

public class KillZone : MonoBehaviour
{
    [Header("SFX")]
    [SerializeField] AudioSource killSound = null;

    void Start()
    {
        GetComponent<MeshRenderer>().enabled = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            killSound.Play(0);
            GameManager.instance.RespawnPlayer();
        }
    }
}