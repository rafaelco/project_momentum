﻿using UnityEngine;

public class CheckpointZone : MonoBehaviour
{
    [SerializeField] Checkpoint checkpoint = null;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            checkpoint.NewCheckpoint();
        }
    }
}