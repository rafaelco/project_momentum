﻿using UnityEngine;
using UnityEngine.UI;

public class Fade : MonoBehaviour
{
    public void FadeIn(float seconds)
    {
        GetComponent<Image>().enabled = true;
        GetComponent<Image>().CrossFadeAlpha(1, seconds, false);
    }

    public void FadeOut(float timeToFadeOut)
    {
        GetComponent<Image>().CrossFadeAlpha(0, timeToFadeOut, false);
    }
}