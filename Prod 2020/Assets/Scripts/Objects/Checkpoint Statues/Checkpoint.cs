﻿using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    [SerializeField] bool startingCheckpoint = false;
    public Transform respawnLocation = null;
    [SerializeField] Canvas logoCanvas = null;
    [SerializeField] GameObject statue = null;

    [Header("SFX")]
    [SerializeField] AudioSource checkpointSound = null;

    void Start()
    {
        if (startingCheckpoint)
        {
            ShowStatue();
        }
        else
        {
            HideStatue();
        }
    }

    void Update()
    {
        //this old code will make the logo canvas look at the player no matter the angle.
        //logoCanvas.transform.rotation = Quaternion.LookRotation(logoCanvas.transform.position - GameManager.instance.player.transform.position);

        //This code will make the logo canvas look at the player on only the y rotation
        Vector3 toTargetVector = GameManager.instance.player.transform.position - logoCanvas.transform.position;
        float yRotation = (Mathf.Atan2(toTargetVector.x, toTargetVector.z) * Mathf.Rad2Deg) - 180f;
        logoCanvas.transform.rotation = Quaternion.Euler(new Vector3(0, yRotation, 0));
    }

    public void NewCheckpoint()
    {
        //Checks if already assigned
        if (GameManager.instance.spawnPoint != this)
        {
            //Hides checkpoint of previous statue.
            GameManager.instance.spawnPoint.HideStatue();

            //Sets self as the player's last checkpoint
            GameManager.instance.spawnPoint = this;
            ShowStatue();
        }
    }

    void ShowStatue()
    {
        checkpointSound.Play(0);
        logoCanvas.enabled = false;
        statue.SetActive(true);
    }

    public void HideStatue()
    {
        logoCanvas.enabled = true;
        statue.SetActive(false);
    }
}