﻿using DG.Tweening; //To access the Tweening Library
using System.Collections; //To use Coroutines
using UnityEngine; //Everything else

public class BridgeScript : MonoBehaviour
{
    //Reminder: Put this on the inspector so nobody except the programmers has to open this script file
    //Tip 1: Make the platform you want to move a child of an empty game object so that empty game object can act as a pivot point for the platform itself

    [Header("Hack Fix")]
    [SerializeField] bool bypassSounds = false;

    [Header("Platform Functions")]
    [Tooltip("Ticking this option will make the platform move from one point to another. Make sure to assign waypoints or else the game will crash")]
    [SerializeField] bool bridgeMovesOnStart = false;
    [SerializeField] bool loops = false;

    [Header("Moving Platform Properties")]
    //Inspector Variables
    [Tooltip("Preferably put an empty game object to use as a waypoint so that the platform can go from one point to another")]
    [SerializeField] GameObject[] waypoints = null;
    [SerializeField] int currentDestination = 0;
    [Tooltip("How long the platform will move from one waypoint to another")]
    [SerializeField] float platformMoveTime = 0f;
    [Tooltip("How long the platform will pause on a waypoint on arrival")]
    [SerializeField] float pauseOnEachWaypoint = 0f;
    //Public Variables
    public bool isPaused = false; //This is used to stop the platform in place. Not flexible yet as you can't stop a platform during mid-travel
    //Private Variables
    //private float distanceFromPlatformToWayPoint = 1f; //This is used to make sure that the platform will reach the destination before moving to another point //This is now unused. It will be kept here for future reference.

    [Header("SFX")]
    [SerializeField] AudioSource bridgeMovingSound = null;
    [SerializeField] AudioSource bridgeConnectedSound = null;

    void Start()
    {
        if (bridgeMovesOnStart)
        {
            StartCoroutine(MovePlatform());
        }
    }

    public void MoveTheBridge()
    {
        StartCoroutine(MovePlatform());
    }

    IEnumerator MovePlatform()
    {
        while (!isPaused)
        {
            while (transform.position != waypoints[currentDestination].transform.position)
            {
                if (!bypassSounds)
                {
                    bridgeMovingSound.Play(0);
                }
                Tween platformMoveTween = transform.DOMove(waypoints[currentDestination].transform.position, platformMoveTime);
                yield return platformMoveTween.WaitForCompletion();
                yield return new WaitForSeconds(pauseOnEachWaypoint);
            }

            if (waypoints.Length > currentDestination)
            {
                currentDestination++;

                if (waypoints.Length <= currentDestination)
                {
                    if (loops)
                    {
                        currentDestination = 0;
                    }
                    else
                    {
                        if (!bypassSounds)
                        {
                            bridgeMovingSound.Stop();
                            bridgeConnectedSound.Play();
                        }

                        isPaused = true;
                    }
                }
            }

            else
            {
                StopCoroutine(MovePlatform());
            }
            yield return null;
        }
        yield return null;


        ////this is called to make sure the project does not nuke itself. DO NOT REMOVE
        //yield return new WaitForSeconds(Time.deltaTime);
        //
        ////This is used to pause the platform in place. Not the best solution but it works
        //if (!isPaused)
        //{
        //    for (int i = 0; i < waypoints.Length; i++)
        //    {
        //        //This conditional will check if the platform is currently moving from one waypoint to another or not
        //        if (Vector3.Distance(waypoints[i].transform.position, transform.position) >= distanceFromPlatformToWayPoint)
        //        {
        //            //This will move a waypoint from its current location to its nex destination
        //            Tween platformMoveTween = transform.DOMove(waypoints[i].transform.position, platformMoveTime);
        //            //This is to make sure the code will not move another step until the platform reaches its destination
        //            yield return platformMoveTween.WaitForCompletion();
        //            //This is to make sure that the platform stays in place until the pause has ended
        //            yield return new WaitForSeconds(pauseOnEachWaypoint);
        //
        //            if (!loops)
        //            {
        //                //EXPERIMENTAL: Due to changes within the project, I had to hastily come up with this "solution"
        //                i = waypoints.Length + 1; //This is to make sure the platform does not loop back to its starting position
        //                isPaused = true; //This is to make sure the platform does not move anymore
        //            }
        //        }
        //    }
        //}
    }

    //This function is called when it collides with something
    void OnCollisionEnter(Collision collision)
    {
        //checks if the collision detected was from the player jumping on the platform
        if (collision.gameObject.CompareTag("Player"))
        {
            //makes the player a child of the platform so that the player will move along the platform if its on top of it
            collision.gameObject.transform.SetParent(gameObject.transform);
            collision.gameObject.GetComponent<Rigidbody>().interpolation = RigidbodyInterpolation.None;
        }
    }

    //This function is called when the collided object is not colliding anymore
    void OnCollisionExit(Collision collision)
    {
        //checks if the collider that left is the player and it also checks if the player is a child of the platform
        if (collision.gameObject.CompareTag("Player") && collision.gameObject.transform.IsChildOf(gameObject.transform))
        {
            //unparents the player from the platform and parents it back to the scene
            collision.gameObject.transform.SetParent(gameObject.transform.root);
            collision.gameObject.GetComponent<Rigidbody>().interpolation = RigidbodyInterpolation.Interpolate;
        }
    }
}