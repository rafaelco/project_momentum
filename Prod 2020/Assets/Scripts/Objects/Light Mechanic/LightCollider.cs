﻿using UnityEngine;

public class LightCollider : MonoBehaviour
{
    [SerializeField] SpecialLight specialLight = null;

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<LightPlatform>())
        {
            specialLight.PlatformInLight(other);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<LightPlatform>())
        {
            specialLight.PlatformOutLight(other);
        }
    }
}