﻿using System.Collections;
using UnityEngine;

public class LightPlatform : MonoBehaviour
{
    [SerializeField] float collisionTimer = 0.4f;
    [SerializeField] Collider colliderForPlayer = null;
    [SerializeField] float speedVFX = 2f;
    [SerializeField] float transistionCurrent = 0f;
    [SerializeField] float transistionDestination = 0f;

    IEnumerator coroutineTurnOffCollision = null;

    //Private Variables
    Material materialLightPlatform = null;

    [Header("SFX")]
    [SerializeField] AudioSource lightplatformSound = null;

    void Start()
    {
        //Variable Storage
        materialLightPlatform = GetComponent<Renderer>().material;

        //Will mark the platform as out of light.
        OutLight();

        //Doesn't allow the player to interact with the true collider of the light platform (the true collider is detected by special lights)
        //Doesn't allow "Collision Direction" to interact with the true collider.
        Physics.IgnoreCollision(GameManager.instance.player.GetComponent<Collider>(), GetComponent<Collider>());
        Physics.IgnoreCollision(GameManager.instance.player.collisionDirection, GetComponent<Collider>());
        Physics.IgnoreCollision(GameManager.instance.player.collisionRightHand, GetComponent<Collider>());

        materialLightPlatform.SetFloat("Vector1_2919F9D5", 1f);
    }

    public void InLight()
    {
        colliderForPlayer.enabled = true;
        StopCoroutine(coroutineTurnOffCollision);
        StartCoroutine(TransitionEffect(0));
    }

    public void OutLight()
    {
        StartCoroutine(TransitionEffect(1));
        coroutineTurnOffCollision = TurnOffCollision();
        StartCoroutine(coroutineTurnOffCollision);
    }

    IEnumerator TransitionEffect(float destination)
    {
        transistionDestination = destination;

        lightplatformSound.Play(0);

        while (transistionCurrent != transistionDestination)
        {
            transistionCurrent = Mathf.MoveTowards(transistionCurrent, transistionDestination, Time.deltaTime * speedVFX);
            materialLightPlatform.SetFloat("Vector1_2919F9D5", transistionCurrent);

            yield return null;
        }
        yield return null;
    }

    IEnumerator TurnOffCollision()
    {
        float time = 0f;
        while (time < collisionTimer)
        {
            time += Time.deltaTime;
            yield return null;
        }
        colliderForPlayer.enabled = false;

        yield return null;
    }
}