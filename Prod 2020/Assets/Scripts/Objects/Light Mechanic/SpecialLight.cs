﻿using UnityEngine;

public class SpecialLight : MonoBehaviour
{
    //[Header("If this doesn't work, it's because there is no Game Manager with an Assigned Player Ability")] //TODO: CHECK LIGHT PLATFORM
    [SerializeField] LightPlatform lightPlatform = null;
    [SerializeField] LightCollider lightCollider = null;
    public bool lightIsOn = true;

    void Start()
    {
        if (lightIsOn)
        {
            TurnOnTheLight();
        }
        else
        {
            TurnOffTheLight();
        }
    }

    public void TurnOnTheLight()
    {
        lightIsOn = true;
        lightCollider.GetComponent<Collider>().enabled = true;
    }

    public void TurnOffTheLight()
    {
        if (lightPlatform != null)
        {
            lightPlatform.OutLight();
            lightPlatform = null;
        }

        lightIsOn = false;
        lightCollider.GetComponent<Collider>().enabled = false;
    }

    #region Light Platform Behavior
    public void PlatformInLight(Collider other)
    {
        if (lightIsOn)
        {
            lightPlatform = other.GetComponent<LightPlatform>();
            lightPlatform.InLight();
        }
    }

    public void PlatformOutLight(Collider other)
    {
        lightPlatform = other.GetComponent<LightPlatform>();
        lightPlatform.OutLight();
        lightPlatform = null;
    }
    #endregion
}