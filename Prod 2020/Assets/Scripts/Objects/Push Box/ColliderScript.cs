﻿using UnityEngine;

public class ColliderScript : MonoBehaviour
{
    [SerializeField] PushBox attachedToBox = null;
    [SerializeField] Direction ColliderDirection = Direction.NORTH;

    public enum Direction
    {
        NORTH, 
        SOUTH,
        WEST,
        EAST
    }

    void OnCollisionEnter(Collision collision)
    {
        attachedToBox.HasBeenHit(ColliderDirection);
    }
}