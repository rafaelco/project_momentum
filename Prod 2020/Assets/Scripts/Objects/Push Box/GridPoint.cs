﻿using System.Collections;
using UnityEngine;

public class GridPoint : MonoBehaviour
{
    [Header("Note: Don't touch any of the component of this object for any reason. The entire grid will break if you do.", order = 1)]
    [Header("Ray Distance controls the maximum lenght that GridPoint can detect one another. Turn on debug mode to see the ray distance in scene view. TURN ON GIZMOS!", order = 2)]
    [Header("In the level, please turn off debug mode if you do not need it.", order = 2)]
    [SerializeField] float rayDistance = 4f;
    [SerializeField] bool debugMode = false;
    [SerializeField] GridPoint pointUp;
    [SerializeField] GridPoint pointDown;
    [SerializeField] GridPoint pointLeft;
    [SerializeField] GridPoint pointRight;

    void Awake()
    {
        //Check for neighboring gridpoints.
        RaycastHit hit;
        //Ignores all but gridpoints.
        int layerMask = 1 << 9;

        #region Check GridPoints
        //Check the UP GridPoint
        Ray myRay1 = new Ray(transform.position, Vector3.forward);

        if (Physics.Raycast(myRay1, out hit, rayDistance, layerMask))
        {
            pointUp = hit.collider.GetComponent<GridPoint>();
        }

        //Check the DOWN GridPoint
        Ray myRay2 = new Ray(transform.position, Vector3.back);
        if (Physics.Raycast(myRay2, out hit, rayDistance, layerMask))
        {
            pointDown = hit.collider.GetComponent<GridPoint>();
        }

        //Check the LEFT GridPoint
        Ray myRay3 = new Ray(transform.position, Vector3.left);
        if (Physics.Raycast(myRay3, out hit, rayDistance, layerMask))
        {
            pointLeft = hit.collider.GetComponent<GridPoint>();
        }

        //Check the RIGHT GridPoint
        Ray myRay4 = new Ray(transform.position, Vector3.right);
        if (Physics.Raycast(myRay4, out hit, rayDistance, layerMask))
        {
            pointRight = hit.collider.GetComponent<GridPoint>();
        }
        #endregion
    }

    void Start()
    {
        //Make Gridpoint Invisible
        Renderer rend = GetComponent<Renderer>();
        rend.enabled = false;

        //Make Gridpoint Untouchable
        Collider col = GetComponent<Collider>();
        col.enabled = false;

        if (debugMode)
        {
            StartCoroutine(DebugDistance());
        }
    }

    public void GoUp(PushBox sourceBox, ChargeMode momentum)
    {
        if (pointUp != null)
        {
            //Check the player's momentum. If it's higher than 0, substract 1 and check the next gridpoint. Otherwise, send this GridPoint to the PushBox.
            if (momentum > 0)
            {
                momentum--;
                pointUp.GoUp(sourceBox, momentum);
            }
            else
            {
                //If the Box no longer has any momentum here, the box will move here.
                StartCoroutine(sourceBox.LerpPosition(this));
            }
        }
        else
        {
            //If there are no gridpoints in the direction, the box will move here.
            StartCoroutine(sourceBox.LerpPosition(this));
        }
    }

    public void GoDown(PushBox sourceBox, ChargeMode momentum)
    {
        if (pointDown != null)
        {
            //Check the player's momentum. If it's higher than 0, substract 1 and check the next gridpoint. Otherwise, send this GridPoint to the PushBox.
            if (momentum > 0)
            {
                momentum--;
                pointDown.GoDown(sourceBox, momentum);
            }
            else
            {
                //If the Box no longer has any momentum here, the box will move here.
                StartCoroutine(sourceBox.LerpPosition(this));
            }
        }
        else
        {
            //If there are no gridpoints in the direction, the box will move here.
            StartCoroutine(sourceBox.LerpPosition(this));
        }
    }

    public void GoLeft(PushBox sourceBox, ChargeMode momentum)
    {
        if (pointLeft != null)
        {
            //Check the player's momentum. If it's higher than 0, substract 1 and check the next gridpoint. Otherwise, send this GridPoint to the PushBox.
            if (momentum > 0)
            {
                momentum--;
                pointLeft.GoLeft(sourceBox, momentum);
            }
            else
            {
                //If the Box no longer has any momentum here, the box will move here.
                StartCoroutine(sourceBox.LerpPosition(this));
            }
        }
        else
        {
            //If there are no gridpoints in the direction, the box will move here.
            StartCoroutine(sourceBox.LerpPosition(this));
        }
    }

    public void GoRight(PushBox sourceBox, ChargeMode momentum)
    {
        if (pointRight != null)
        {
            //Check the player's momentum. If it's higher than 0, substract 1 and check the next gridpoint. Otherwise, send this GridPoint to the PushBox.
            if (momentum > 0)
            {
                momentum--;
                pointRight.GoRight(sourceBox, momentum);
            }
            else
            {
                //If the Box no longer has any momentum here, the box will move here.
                StartCoroutine(sourceBox.LerpPosition(this));
            }
        }
        else
        {
            //If there are no gridpoints in the direction, the box will move here.
            StartCoroutine(sourceBox.LerpPosition(this));
        }
    }

    //DEBUG: Only use this when debugging ray distance.
    IEnumerator DebugDistance()
    {
        while (debugMode)
        {
            Debug.DrawRay(transform.position, Vector3.forward * rayDistance, Color.green);
            Debug.DrawRay(transform.position, Vector3.back * rayDistance, Color.green);
            Debug.DrawRay(transform.position, Vector3.left * rayDistance, Color.green);
            Debug.DrawRay(transform.position, Vector3.right * rayDistance, Color.green);
            yield return null;
        }
        yield return null;
    }
}