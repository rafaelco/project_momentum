﻿using System.Collections;
using UnityEngine;

public class PushBox : MonoBehaviour
{
    [Header("HOW TO PLACE:", order = 0)]
    [Header("Note: This will have to be redone later when we have the grid floors assets.", order = 1)]
    [Header("Step 1: Place the object Grid where you want. Don't modify the coordinates of the PushBox at this point, just Grid.", order = 2)]
    [Header("Step 2: Once you have chosen the initial position of PushBox. You can add GridPoints (child of Grid) to where you want the Push Box to be able to move.", order = 3)]
    [Header("All Gridpoints must be at an equal distance from one another. You should use position coordinates for this.", order = 4)]
    [Header("HOW TO USE:", order = 5)]
    [Header("Create an Empty GameObject called Game Manager and add the Game Manager Component if one doesn't exist.", order = 6)]

    [Header("Initial Grid Point", order = 11)]
    [SerializeField] GridPoint pointCurrent = null;

    [Header("This controls the time it takes for the push box to reach its destination, depending on the player's momentum.", order = 12)]
    [SerializeField] float speedLow = 0.1f;
    [SerializeField] float speedMedium = 0.2f;
    [SerializeField] float speedHigh = 0.3f;

    bool isMoving = false;

    public void HasBeenHit(ColliderScript.Direction collisionDirection)
    {
        if(!isMoving)
        {
            ////Check Direction
            //Up
            if (collisionDirection == ColliderScript.Direction.NORTH)
            {
                pointCurrent.GoDown(this, GameManager.instance.player.currentMomentum);
            }

            //Down
            if (collisionDirection == ColliderScript.Direction.SOUTH)
            {
                pointCurrent.GoUp(this, GameManager.instance.player.currentMomentum);
            }

            //Left
            if (collisionDirection == ColliderScript.Direction.WEST)
            {
                pointCurrent.GoRight(this, GameManager.instance.player.currentMomentum);
            }

            //Right
            if (collisionDirection == ColliderScript.Direction.EAST)
            {
                pointCurrent.GoLeft(this, GameManager.instance.player.currentMomentum);
            }
        }
    }

    public IEnumerator LerpPosition(GridPoint newGridPoint)
    {
        float time = 0;
        Vector3 startPosition = transform.position;
        isMoving = true;
        float lerpTime;
        
        switch (GameManager.instance.player.currentMomentum)
        {
            case ChargeMode.LOW:
                lerpTime = speedLow;
                break;
            case ChargeMode.MEDIUM:
                lerpTime = speedMedium;
                break;
            case ChargeMode.HIGH:
                lerpTime = speedHigh;
                break;
            default:
                lerpTime = speedHigh;
                break;
        }

        while (time < lerpTime)
        {
            transform.position = Vector3.Lerp(startPosition, newGridPoint.transform.position, time / lerpTime);
            time += Time.deltaTime;
            yield return null;
        }
        transform.position = newGridPoint.transform.position;
        pointCurrent = newGridPoint;
        isMoving = false;
    }
}