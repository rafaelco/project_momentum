﻿using UnityEngine;

public class SimpleTrigger : MonoBehaviour
{
    [Header("Activation Status")]
    [SerializeField] bool hasBeenActivated = false;

    [Header("Gameplay Objects")]
    [SerializeField] Laser[] lasersToTurnOn = null;
    [SerializeField] Laser[] lasersToTurnOff = null;
    [SerializeField] Door[] doorsToOpen = null;
    [SerializeField] Door[] doorsToClose = null;
    [SerializeField] SpecialLight[] lightsToTurnOn = null;
    [SerializeField] SpecialLight[] lightsToTurnOff = null;
    [SerializeField] PistonBehavior[] pistonsToActivate = null;
    [SerializeField] PistonBehavior[] pistonsToDeactivate = null;
    [SerializeField] BridgeScript[] bridges = null;
    [SerializeField] RampScript[] ramps = null;
    [SerializeField] ChargeMode newRampMode = ChargeMode.HIGH;

    void OnTriggerEnter(Collider player)
    {
        if (!hasBeenActivated)
        {
            Activate();
        }
    }

    void Activate()
    {
        //Lasers
        if (lasersToTurnOn.Length > 0)
        {
            for (int i = 0; i < lasersToTurnOn.Length; i++)
            {
                lasersToTurnOn[i].TurnOnTheLaser();
            }
        }
        if (lasersToTurnOff.Length > 0)
        {
            for (int i = 0; i < lasersToTurnOff.Length; i++)
            {
                lasersToTurnOff[i].TurnOffTheLaser();
            }
        }

        //Doors
        if (doorsToOpen.Length > 0)
        {
            for (int i = 0; i < doorsToOpen.Length; i++)
            {
                doorsToOpen[i].OpenTheDoor();
            }
        }
        if (doorsToClose.Length > 0)
        {
            for (int i = 0; i < doorsToClose.Length; i++)
            {
                doorsToClose[i].CloseTheDoor();
            }
        }

        //Bridges
        if (bridges.Length > 0)
        {
            for (int i = 0; i < bridges.Length; i++)
            {
                bridges[i].MoveTheBridge();
            }
        }

        //Lights
        if (lightsToTurnOn.Length > 0)
        {
            for (int i = 0; i < lightsToTurnOn.Length; i++)
            {
                lightsToTurnOn[i].TurnOnTheLight();
            }
        }
        if (lightsToTurnOff.Length > 0)
        {
            for (int i = 0; i < lightsToTurnOff.Length; i++)
            {
                lightsToTurnOff[i].TurnOffTheLight();
            }
        }

        //Pistons
        if (pistonsToActivate.Length > 0)
        {
            for (int i = 0; i < pistonsToActivate.Length; i++)
            {
                pistonsToActivate[i].StartPistonFunction();
            }
        }
        if (pistonsToDeactivate.Length > 0)
        {
            for (int i = 0; i < pistonsToDeactivate.Length; i++)
            {
                pistonsToDeactivate[i].StopPistonFunction();
            }
        }

        if (ramps.Length > 0)
        {
            for (int i = 0; i < ramps.Length; i++)
            {
                ramps[i].rampMode = newRampMode;
                ramps[i].RampModeSelection();
            }
        }

        hasBeenActivated = true;
    }
}