﻿using UnityEngine;

public class PhareRotation : MonoBehaviour
{
    [SerializeField] float xSpeed = 0f;
    [SerializeField] float ySpeed = 15f;
    [SerializeField] float zSpeed = 0f;
    [SerializeField] bool isClockWise = false;

    void Start()
    {
        if (isClockWise)
        {
            ySpeed = -ySpeed;
        }
    }

    void FixedUpdate()
    {
        gameObject.transform.Rotate(new Vector3(xSpeed, ySpeed, zSpeed) * Time.deltaTime);
    }
}