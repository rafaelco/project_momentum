﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightBeamDeactivation : MonoBehaviour
{
    public GameObject beamDeactivate;
    public GameObject beamActivate;

    private void Update()
    {
        if(gameObject.GetComponent<Light>().enabled == false)
        {
            beamDeactivate.SetActive(false);
            beamActivate.SetActive(true);
            gameObject.GetComponent<LightBeamDeactivation>().enabled = false;
        }
    }
}
