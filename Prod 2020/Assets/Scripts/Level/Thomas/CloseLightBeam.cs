﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseLightBeam : MonoBehaviour
{
    public GameObject beamDeactivate;

    private void Update()
    {
        if (gameObject.GetComponent<Light>().enabled == false)
        {
            beamDeactivate.SetActive(false);
            gameObject.GetComponent<CloseLightBeam>().enabled = false;
        }
    }
}
