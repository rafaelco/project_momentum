﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingLight : MonoBehaviour
{
    public float ShticRotationSpeed = 20f;

    public void Update()
    {
        this.transform.Rotate(new Vector3(0f, ShticRotationSpeed, 0f) * Time.deltaTime);
    }
}
