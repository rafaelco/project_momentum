﻿using UnityEngine;

public class CloneRotation : MonoBehaviour
{
    [SerializeField] bool applyRandomRotation = true;
    
    void Awake()
    {
        if (applyRandomRotation)
        {
            transform.rotation = Quaternion.Euler(-90, 0, Random.Range(0, 360));
        }
    }
}