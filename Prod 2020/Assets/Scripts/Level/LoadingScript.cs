﻿using UnityEngine;

public class LoadingScript : MonoBehaviour
{
    [SerializeField] bool deleteAfterUse = false;
    [SerializeField] bool isStartArea = false;
    [SerializeField] GameObject[] AreasToDespawn = null;
    [SerializeField] GameObject[] AreasToSpawn = null;

    void Awake()
    {
        if (isStartArea)
        {
            for (int i = 0; i < AreasToDespawn.Length; i++)
            {
                AreasToDespawn[i].SetActive(false);
            }

            for (int i = 0; i < AreasToSpawn.Length; i++)
            {
                AreasToSpawn[i].SetActive(true);
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        for (int i = 0; i < AreasToSpawn.Length; i++)
        {
            AreasToSpawn[i].SetActive(true);
        }

        for (int i = 0; i < AreasToDespawn.Length; i++)
        {
            AreasToDespawn[i].SetActive(false);
        }

        if (deleteAfterUse)
        {
            Destroy(gameObject);
        }
    }
}