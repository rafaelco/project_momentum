
For the Artists 
when you bring your work in Unity → Push 

Create a folder INSIDE the Art folder → choose the corresponding folder 
* Character, Environment, Modular Assets, Props, Savepoint NPC
i.e : Assets/Art/Props/SlidingDoor

Create these folders inside your folder 
* Materials, Textures, Model 
i.e : Assets/Art/Props/SlidingDoor/Materials
i.e : Assets/Art/Props/SlidingDoor/Textures
i.e : Assets/Art/Props/SlidingDoor/Model

####### NAMING CONVENTION ##########

follow these guide lines for the naming of your assets : 
* TypeOfObject_NameOfObject
i.e: Prop_SlidingDoor

* TypeOfObject_NameOfObject_TextureType
i.e: Prop_SlidingDoor_Albedo
i.e: Prop_SlidingDoor_Emission
i.e: Prop_SlidingDoor_MetallicSmoothness
i.e: Prop_SlidingDoor_Normal

* TypeOfObject_nameOfObject_mat + (_suffix) if needed
ie: Prop_SlidingDoor_mat

Refere to Notes.png for visual representation 